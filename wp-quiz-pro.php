<?php
/*
 * Plugin Name: WP Quiz Pro
 * Plugin URI:  https://mythemeshop.com/plugins/wp-quiz-pro/
 * Description: WP Quiz Pro lets you easily add polished, responsive and modern quizzes to your site or blog! Increase engagement and shares while building your mailing list! WP Quiz Pro makes it easy!
 * Version:     1.0.3
 * Author:      MyThemeShop
 * Author URI:  https://mythemeshop.com/
 *
 * Text Domain: wp-quiz-pro
 * Domain Path: /languages/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // disable direct access
}

if ( ! class_exists( 'WP_Quiz_Pro_Plugin' ) ) :


/**
 * Register the plugin.
 *
 * Display the administration panel, insert JavaScript etc.
 */
class WP_Quiz_Pro_Plugin {

	/**
     * @var string
     */
    public $version = "1111";

	/**
	 * @var WP Quiz
	 */
	public $quiz = null;

	/**
	 * Init
	 */
	public static function init() {

		$wp_quiz = new self();


	}

	/**
     * Constructor
     */
    public function __construct() {

		$this->define_constants();
		$this->includes();
		$this->setup_actions();
		$this->setup_filters();
		$this->setup_shortcode();

	}

	/**
	 * Define WP Quiz constants
	 */
	private function define_constants() {

		define( 'WP_QUIZ_PRO_VERSION',    $this->version );
		define( 'WP_QUIZ_PRO_BASE_URL',   trailingslashit( plugins_url( 'wp-quiz-pro' ) ) );
		define( 'WP_QUIZ_PRO_ASSETS_URL', trailingslashit( WP_QUIZ_PRO_BASE_URL . 'assets' ) );
		define( 'WP_QUIZ_PRO_PATH',       plugin_dir_path( __FILE__ ) );

	}

	/**
     * Load required classes
	 */
    private function includes() {

		//auto loader
		spl_autoload_register( array( $this, 'autoloader' ) );
	}

	/**
     * Autoload classes
     */
    public function autoloader( $class ) {

        $dir = WP_QUIZ_PRO_PATH . 'inc' . DIRECTORY_SEPARATOR;
		$class_file_name = 'class-' . str_replace( array( 'wp_quiz_pro_', '_' ), array( '', '-' ), strtolower( $class ) ) . '.php';
		if ( file_exists( $dir . $class_file_name ) ) {
			require $dir . $class_file_name;
		}

	}

	/**
     * Register the [wp_quiz_pro] shortcode.
     */
	private function setup_shortcode() {

		add_shortcode( 'wp_quiz_pro', array( $this, 'register_shortcode' ) );
	}

	/**
	 * Hook WP Quiz into WordPress
	 */
	private function setup_actions() {

		add_action( 'admin_menu', array( $this, 'register_admin_menu' ), 10 );
		add_action( 'admin_notices', array( $this, 'wp_quiz_imagick_admin_notice' ) );
		add_action( 'wp_head', array( $this, 'wp_quiz_pro_head' ), 1 );
		add_action( 'wp_quiz_admin_page', array( $this, 'admin_page_players' ), 2 );
		add_action( 'wp_quiz_admin_page', array( $this, 'admin_page_email_subs' ), 3 );
		add_action( 'wp_quiz_admin_page', array( $this, 'admin_import_export' ), 4 );

		add_action( 'init', array( $this, 'register_post_type' ) );
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
		add_action( 'init', array( $this, 'wp_quiz_embed_quiz' ) );
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		add_action( 'enter_title_here', array( $this, 'enter_title_here' ) );
		add_action( 'save_post', array( $this, 'save_post' ), 10, 2 );
		add_action( 'manage_wp_quiz_posts_custom_column', array( $this, 'manage_wp_quiz_columns' ), 10, 2 );
		add_action( 'edit_form_after_title', array( $this, 'add_shortcode_before_editor' ) );

		//Save form posts
		add_action( 'admin_post_wp_quiz', array( $this, 'save_post_form' ) );

		//Ajax
		add_action( 'wp_ajax_wq_quizResults', array( $this, 'wp_quiz_pro_results' ) );
		add_action( 'wp_ajax_nopriv_wq_quizResults', array( $this, 'wp_quiz_pro_results' ) );
		add_action( 'wp_ajax_wq_submitInfo', array( $this, 'wp_quiz_pro_user_info' ) );
		add_action( 'wp_ajax_nopriv_wq_submitInfo', array( $this, 'wp_quiz_pro_user_info' ) );
		add_action( 'wp_ajax_wq_submitFbInfo', array( $this, 'wp_quiz_pro_fb_user_info' ) );
		add_action( 'wp_ajax_nopriv_wq_submitFbInfo', array( $this, 'wp_quiz_pro_fb_user_info' ) );

		add_action( 'wp_ajax_check_image_file', array( $this, 'wp_quiz_pro_check_image_file' ) );
		add_action( 'wp_ajax_check_video_file', array( $this, 'wp_quiz_pro_check_video_file' ) );
		add_action( 'wp_ajax_dismiss_imagick_notice', array( $this, 'wp_quiz_pro_dismiss_imagick_notice' ) );
	}

	/**
	 * Hook WP Quiz into WordPress
	 */
	private function setup_filters() {

		add_filter( 'manage_edit-wp_quiz_columns', array(  $this, 'add_new_wp_quiz_columns' ) );
		add_filter( 'screen_layout_columns', array( $this, 'on_screen_layout_columns'), 10, 2 );
		add_filter( 'the_content', array( $this, 'create_quiz_page' ) );
	}

	/**
     * Register Quiz post type
	 */
	public function register_post_type() {

		$labels = array(
			'name'               => __( 'WP Quiz', 'wp-quiz-pro' ),
			'menu_name'          => __( 'WP Quiz Pro', 'wp-quiz-pro' ),
			'singular_name'      => __( 'WP Quiz', 'wp-quiz-pro' ),
			'name_admin_bar'     => _x( 'WP Quiz Pro', 'name admin bar', 'wp-quiz-pro' ),
			'all_items'          => __( 'All Quizzes', 'wp-quiz-pro' ),
			'search_items'       => __( 'Search Quizzes', 'wp-quiz-pro' ),
			'add_new'            => _x( 'Add New', 'quiz', 'wp-quiz-pro' ),
			'add_new_item'       => __( 'Add New Quiz', 'wp-quiz-pro' ),
			'new_item'           => __( 'New Quiz', 'wp-quiz-pro' ),
			'view_item'          => __( 'View Quiz', 'wp-quiz-pro' ),
			'edit_item'          => __( 'Edit Quiz', 'wp-quiz-pro' ),
			'not_found'          => __( 'No Quizzes found.', 'wp-quiz-pro' ),
			'not_found_in_trash' => __( 'WP Quiz not found in Trash.', 'wp-quiz-pro' ),
			'parent_item_colon'  => __( 'Parent Quiz', 'wp-quiz-pro' ),
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Holds the quizzes and their data.', 'wp-quiz-pro' ),
			'menu_position'      => 5,
			'menu_icon'			 => 'dashicons-editor-help',
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt' ),
		);

		register_post_type( 'wp_quiz', $args );

		if ( false === get_option('wp_quiz_pro_version') ) {
			flush_rewrite_rules();
			update_option('wp_quiz_pro_version', WP_QUIZ_PRO_VERSION );
		}
	}

	/**
	 * Add meta boxes
     */
	public function add_meta_boxes(){

		add_meta_box(
			'quiz-content',
			_x( 'Quiz', 'metabox title', 'wp-quiz-pro' ),
			array( $this, 'quiz_content_meta_box' ),
			'wp_quiz',
			'normal',
			'high'
		);

	}

	/**
	 * Add meta boxes content
     */
	public function quiz_content_meta_box() {

		$quiz_type = get_post_meta( get_the_ID(), 'quiz_type', true );

		$fb_quiz = extension_loaded( 'imagick' ) ? 'Facebook Quiz' : 'Facebook Quiz (ImageMagic Not Found)';

		$quiz_types = array(
			'trivia' 		=> __( 'Trivia', 'wp-quiz-pro' ),
			'personality'	=> __( 'Personality', 'wp-quiz-pro' ),
			'swiper'		=> __( 'Swiper', 'wp-quiz-pro' ),
			'flip'			=> __( 'Flip Cards', 'wp-quiz-pro' ),
			'fb_quiz'		=> __( $fb_quiz, 'wp-quiz-pro' )
		);

		$animations = array(
			'fade','scale', 'fade up', 'fade down', 'fade left', 'fade right', 'horizontal flip', 'vertical flip',
			'drop', 'fly left', 'fly right', 'fly up', 'fly down', 'swing left', 'swing right', 'swing up',
			'swing down', 'browse', 'browse right', 'slide down', 'slide up', 'slide left', 'slide right'
		);

		$fonts = array();
		if( class_exists( 'Imagick' ) ) {
			$imagick 	= new Imagick();
			$fonts 		= $imagick->queryFonts();
		}

		$share_buttons = array(
			'fb' => __( 'Facebook', 'wp-quiz-pro' ),
			'tw' => __( 'Twitter', 'wp-quiz-pro' ),
			'g+' => __( 'Google +', 'wp-quiz-pro' ),
			'vk' => __( 'VK', 'wp-quiz-pro' )
		);

		$defaults = get_option( 'wp_quiz_pro_default_settings' );
		unset( $defaults[ 'analytics' ] );
		unset( $defaults[ 'mail_service' ] );
		unset( $defaults[ 'mailchimp' ] );
		unset( $defaults[ 'getresponse' ] );
		unset( $defaults[ 'ad_code' ] );
		unset( $defaults[ 'share_meta' ] );
		unset( $defaults[ 'players_tracking' ] );

		foreach( $defaults as $key => $value ){

			$defaults[ $key ][ 'question_layout' ] 	= 'single';
			$defaults[ $key ][ 'skin' ] 			= 'flat';
			$defaults[ $key ][ 'bar_color' ]		= '#00c479';
			$defaults[ $key ][ 'font_color' ]		= '#444';
			$defaults[ $key ][ 'background_color' ]	= '';
			$defaults[ $key ][ 'animation_in' ] 	= 'fade';
			$defaults[ $key ][ 'animation_out' ]	= 'fade';
			$defaults[ $key ][ 'size' ]				= 'full';
			$defaults[ $key ][ 'custom_width' ]		= '338';
			$defaults[ $key ][ 'custom_height' ]	= '468';
			$defaults[ $key ][ 'profile' ]			= 'user';
			$defaults[ $key ][ 'title_size' ]		= '20';
			$defaults[ $key ][ 'title_font' ]		= 'Helvetica-Bold';
			$defaults[ $key ][ 'share_buttons' ]	= array( 'fb', 'tw', 'g+', 'vk' );
		}

		wp_localize_script( 'wp_quiz_pro-react', 'quiz', array(
			'types' 			=> $quiz_types,
			'typeSelected' 		=> '' === $quiz_type ? 'trivia' : $quiz_type,
			'nonce' 			=> wp_create_nonce( 'quiz-content' ),
			'questions' 		=> get_post_meta( get_the_ID(), 'questions', true ),
			'results' 			=> get_post_meta( get_the_ID(), 'results', true ),
			'settings' 			=> get_post_meta( get_the_ID(), 'settings', true),
			'defaultSettings' 	=> $defaults[ 'defaults' ],
			'animations'		=> $animations,
			'shareButtons'		=> $share_buttons,
			'fonts'				=> $fonts,
			'profileUrl'		=> WP_QUIZ_PRO_ASSETS_URL . 'image/avatar.jpg',
			'titleUrl'			=> WP_QUIZ_PRO_ASSETS_URL . 'image/title.png',
			'defaultSkins'		=> array( 'flat' => '#ecf0f1', 'trad' => '#f2f2f2' ),
			'imagickActive'		=> extension_loaded( 'imagick' ) ? 'true' : 'false'
		) );
		?>
			<script type="text/javascript">
				jQuery(document).ready(function ($) {
					$('#tabs').tab();
					$('.color-field').wpColorPicker();
				});
			</script>
		<?php
	}

	/**
     * Register admin JavaScript
     */
	public function admin_enqueue_scripts( $hook ){

		global $typenow;

		if( $typenow !== "wp_quiz" ){
			return;
		}

		if ( $hook == "post.php" || $hook == "post-new.php" ) {
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'wp-quiz-jquery-ui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' );
			wp_enqueue_style( 'wp_quiz_pro-css', WP_QUIZ_PRO_ASSETS_URL . 'css/new-quiz.css', array(), WP_QUIZ_PRO_VERSION );
			wp_enqueue_style( 'semantic-checkbox-css', WP_QUIZ_PRO_ASSETS_URL . 'css/checkbox.min.css', array(), WP_QUIZ_PRO_VERSION );
			wp_enqueue_style( 'chosen-css', WP_QUIZ_PRO_ASSETS_URL . 'css/chosen.min.css', array(), WP_QUIZ_PRO_VERSION );
			wp_enqueue_style( 'semantic-embed-css', WP_QUIZ_PRO_ASSETS_URL . 'css/embed.min.css', array(), WP_QUIZ_PRO_VERSION );
			wp_enqueue_style( 'tipsy-css', WP_QUIZ_PRO_ASSETS_URL . 'css/tipsy.css', array(), WP_QUIZ_PRO_VERSION );

			wp_enqueue_script( 'jquery-ui-draggable' );
			wp_enqueue_script( 'jquery-ui-resizable' );
			wp_enqueue_script( 'wp_quiz_pro-react', WP_QUIZ_PRO_ASSETS_URL . 'js/content.min.js', array( 'jquery', 'semantic-checkbox-js', 'chosen-js', 'wp-color-picker', 'leanmodal-js', 'tipsy-js' ), WP_QUIZ_PRO_VERSION, true );
			wp_enqueue_script( 'wp_quiz_pro-bootstrap', WP_QUIZ_PRO_ASSETS_URL . 'js/bootstrap.min.js', array( 'jquery' ), WP_QUIZ_PRO_VERSION );
			wp_enqueue_script( 'semantic-checkbox-js', WP_QUIZ_PRO_ASSETS_URL . 'js/checkbox.min.js', array( 'jquery' ), WP_QUIZ_PRO_VERSION );
			wp_enqueue_script( 'chosen-js', WP_QUIZ_PRO_ASSETS_URL . 'js/chosen.jquery.min.js', array( 'jquery' ), WP_QUIZ_PRO_VERSION );
			wp_enqueue_script( 'semantic-embed-js', WP_QUIZ_PRO_ASSETS_URL . 'js/embed.min.js', array( 'jquery' ), WP_QUIZ_PRO_VERSION );
			wp_enqueue_script( 'leanmodal-js', WP_QUIZ_PRO_ASSETS_URL . 'js/jquery.leanModal.min.js', array( 'jquery'), WP_QUIZ_PRO_VERSION );
			wp_enqueue_script( 'tipsy-js', WP_QUIZ_PRO_ASSETS_URL . 'js/jquery.tipsy.js', array( 'jquery'), WP_QUIZ_PRO_VERSION );

			wp_localize_script( 'wp_quiz_pro-react', 'wq_l10n', array(
				'labelSelectType' 		=> 	__( 'Select Quiz Type', 'wp-quiz-pro' ),
				'content'				=>  __( 'Content', 'wp-quiz-pro' ),
				'styling'				=>  __( 'Styling', 'wp-quiz-pro' ),
				'settings'				=>  __( 'Settings', 'wp-quiz-pro' ),
				'questions'				=>  __( 'Questions', 'wp-quiz-pro' ),
				'questionSingle'		=>  __( 'Question', 'wp-quiz-pro' ),
				'addQuestion'			=> 	__( 'Add Question', 'wp-quiz-pro' ),
				'editQuestion'			=> 	__( 'Edit Question', 'wp-quiz-pro' ),
				'results'				=>  __( 'Results', 'wp-quiz-pro' ),
				'addResult' 			=> 	__( 'Add Result', 'wp-quiz-pro' ),
				'editResult' 			=> 	__( 'Edit Result', 'wp-quiz-pro' ),
				'addAnswer' 			=> 	__( 'Add Answer', 'wp-quiz-pro' ),
				'editAnswer'			=> 	__( 'Edit Answer', 'wp-quiz-pro' ),
				'addExplanation' 		=> 	__( 'Add Explanation', 'wp-quiz-pro' ),
				'editExplanation'		=> 	__( 'Edit Explanation', 'wp-quiz-pro' ),
				'edit'					=> 	__( 'Edit', 'wp-quiz-pro' ),
				'delete' 				=> 	__( 'Delete', 'wp-quiz-pro' ),
				'question'				=> 	_x( 'Question Title', 'input label', 'wp-quiz-pro' ),
				'result' 				=> 	_x( 'Result Title', 'input label', 'wp-quiz-pro' ),
				'answer' 				=> 	_x( 'Answer Title', 'input label', 'wp-quiz-pro' ),
				'explanation'			=> 	_x( 'Explanation', 'input label', 'wp-quiz-pro' ),
				'image'					=> 	_x( 'Image', 'input label', 'wp-quiz-pro' ),
				'frontImage'			=> 	_x( 'Front Image', 'input label', 'wp-quiz-pro' ),
				'backImage'				=> 	_x( 'Back Image', 'input label', 'wp-quiz-pro' ),
				'backImageDesc'			=> 	_x( 'Back Image Description', 'input label', 'wp-quiz-pro' ),
				'votesUp'				=>  _x( 'Votes Up', 'input label', 'wp-quiz-pro' ),
				'votesDown'				=>  _x( 'Votes Down', 'input label', 'wp-quiz-pro' ),
				'imageCredit'			=>  _x( 'Image Credit', 'input label', 'wp-quiz-pro' ),
				'mediaType' 			=> 	_x( 'Media Type', 'input label', 'wp-quiz-pro' ),
				'videoUrl'				=> 	_x( 'Youtube/Vimeo/Custom URL', 'input label', 'wp-quiz-pro' ),
				'placeholderImage' 		=> 	_x( 'Image Placeholder', 'input label', 'wp-quiz-pro' ),
				'isCorrect' 			=> 	_x( 'Is Correct Answer', 'input label', 'wp-quiz-pro' ),
				'minCorrect' 			=> 	_x( 'Minimum Correct', 'input label', 'wp-quiz-pro' ),
				'maxCorrect'			=> 	_x( 'Maximum Correct', 'input label', 'wp-quiz-pro' ),
				'minScore'				=> 	_x( 'Minimum Score', 'input label', 'wp-quiz-pro' ),
				'maxScore'				=> 	_x( 'Maximum Score', 'input label', 'wp-quiz-pro' ),
				'desc'					=>  _x( 'Description', 'input label', 'wp-quiz-pro' ),
				'shtDesc'				=>  _x( 'Short Description', 'input label', 'wp-quiz-pro' ),
				'pointsResult'			=>  _x( 'Result Points', 'input label', 'wp-quiz-pro' ),
				'pointsExplain'			=>  __( '(Association: 0-no, 1-normal, 2-strong)', 'wp-quiz-pro' ),
				'lngDesc'				=>  _x( 'Long Description', 'input label', 'wp-quiz-pro' ),
				'cancel' 				=> 	__( 'Cancel', 'wp-quiz-pro' ),
				'saveChanges' 			=> 	__( 'Save Changes', 'wp-quiz-pro' ),
				'videoEmbed' 			=> 	__( 'Video/Custom Embed', 'wp-quiz-pro' ),
				'noMedia'				=> 	__( 'No Media', 'wp-quiz-pro' ),
				'generalSettings'		=> 	__( 'General Settings', 'wp-quiz-pro' ),
				'randomizeQuestions'	=> 	__( 'Randomize Questions', 'wp-quiz-pro' ),
				'randomizeAnswers'		=> 	__( 'Randomize Answers', 'wp-quiz-pro' ),
				'restartQuestions'		=> 	__( 'Restart Questions', 'wp-quiz-pro' ),
				'promote'				=> 	__( 'Promote the plugin', 'wp-quiz-pro' ),
				'embedToggle'			=> 	__( 'Show embed code toggle', 'wp-quiz-pro' ),
				'shareButtons'			=> 	__( 'Share buttons', 'wp-quiz-pro' ),
				'countDown'				=>  __( 'Countdown timer [Seconds/question]', 'wp-quiz-pro' ),
				'multipleExplain'		=>  __( '(applies to multiple page layout)', 'wp-quiz-pro' ),
				'autoScroll'			=>  __( 'Auto scroll to next question', 'wp-quiz-pro' ),
				'endAnswers'			=>  __( 'Show right/wrong answers at the end of quiz', 'wp-quiz-pro' ),
				'singleExplain'			=>  __( '(applies to single page layout)', 'wp-quiz-pro' ),
				'forceAction'			=> 	__( 'Force action to see results', 'wp-quiz-pro' ),
				'forceAction0'			=> 	__( 'No Action', 'wp-quiz-pro' ),
				'forceAction1'			=> 	__( 'Capture Email', 'wp-quiz-pro' ),
				'forceAction2'			=> 	__( 'Facebook Share', 'wp-quiz-pro' ),
				'showAds'				=> 	__( 'Show Ads', 'wp-quiz-pro' ),
				'adsAfterN'				=> 	__( 'Ads after every nth question', 'wp-quiz-pro' ),
				'repeatAds'				=>  __( 'Repeat Ads', 'wp-quiz-pro' ),
				'adCodes'				=> 	__( 'Ad Codes', 'wp-quiz-pro' ),
				'adCodesDesc'			=> 	__( 'comma separated  codes', 'wp-quiz-pro' ),
				'customizeLayout'		=> 	__( 'Customize Layout and Colors', 'wp-quiz-pro' ),
				'questionsLayout'		=> 	__( 'Questions layout', 'wp-quiz-pro' ),
				'showAll'				=> 	__( 'Show all', 'wp-quiz-pro' ),
				'mutiplePages'			=> 	__( 'Mutiple pages', 'wp-quiz-pro' ),
				'chooseSkin'			=> 	__( 'Choose skin', 'wp-quiz-pro' ),
				'traditionalSkin'		=> 	__( 'Traditional Skin', 'wp-quiz-pro' ),
				'flatSkin'				=> 	__( 'Modern Flat Skin', 'wp-quiz-pro' ),
				'progressColor'			=> 	__( 'Progress bar color', 'wp-quiz-pro' ),
				'questionColor'			=> 	__( 'Question font color', 'wp-quiz-pro' ),
				'questionBgColor'		=> 	__( 'Question background color', 'wp-quiz-pro' ),
				'titleColor'			=> 	__( 'Result title color', 'wp-quiz-pro' ),
				'titleSize'				=>  __( 'Result title font size', 'wp-quiz-pro' ),
				'titleFont'				=>  __( 'Result title font', 'wp-quiz-pro' ),
				'chooseProfile'			=>  __( 'Select Profile', 'wp-quiz-pro' ),
				'userProfile'			=>  __( 'User Profile Image', 'wp-quiz-pro' ),
				'friendProfile'			=>  __( 'Friend Profile Image', 'wp-quiz-pro' ),
				'animationIn'			=> 	__( 'Animation In', 'wp-quiz-pro' ),
				'animationOut'			=> 	__( 'Animation Out', 'wp-quiz-pro' ),
				'quizSize'				=>  __( 'Quiz Size', 'wp-quiz-pro' ),
				'custom'				=>  __( 'Custom', 'wp-quiz-pro' ),
				'customSize'			=>  __( 'Custom Size', 'wp-quiz-pro' ),
				'width'					=>  __( 'Width:' , 'wp-quiz-pro' ),
				'height'				=>  __( 'Height:' , 'wp-quiz-pro' ),
				'customExplain'			=>  __( 'set width and height in px', 'wp-quiz' ),
				'fullWidth'				=>  __( 'Full Width (responsive)', 'wp-quiz-pro' ),
				'answers'				=> 	__( 'Answers', 'wp-quiz-pro' ),
				'upload'				=>  __( 'Upload', 'wp-quiz-pro' ),
				'uploadImage'			=>  __( 'Upload Image', 'wp-quiz-pro' ),
				'preview'				=>  __( 'Preview', 'wp-quiz-pro' ),
				'previewImage'			=>  __( 'Preview Image', 'wp-quiz-pro' ),
				'previewMedia'			=>  __( 'Preview Video/Media', 'wp-quiz-pro' ),
				'PrePosition'			=>  __( 'Preview/Position', 'wp-quiz-pro' ),
				'prePositionImage'		=>  __( 'Preview Image and set profile position', 'wp-quiz-pro' ),
				'sliderTitle'			=>  __( 'image border radius', 'wp-quiz-pro' ),
				'ajax_url'				=>  admin_url( 'admin-ajax.php' ),
				'personalityNotice'		=> 	__( 'Please add the Results and save the draft before adding questions', 'wp-quiz' ),
				'fbnameNotice'			=>  __( 'Possible name substiution (%%userfirstname%% = user first name, %%lastfirstname%% = user last name, %%friendfirstname%% = friend first name, %%friendfirstname%% = friend last name)', 'wp-quiz-pro' ),
				'fbprofileNotice'		=>  __( 'Friend profile image will only work if the current quiz player/user has Facebook friends that has also authorized your app id to read their friends list.', 'wp-quiz-pro' )
			) );
		}
		add_thickbox();

	}

	public function enter_title_here( $text ) {

		global $typenow;

		if ( ! is_admin() || 'wp_quiz' !== $typenow )
			return $text;

		return _x( 'Quiz Title', 'new quiz title placeholder', 'wp-quiz-pro' );
	}

	public function add_shortcode_before_editor() {
		global $typenow;

		if ( 'wp_quiz' === $typenow && isset( $_GET['post'] ) ) {
			echo '<div class="inside"><strong style="padding: 0 10px;">'.__( 'Shortcode:', 'wp-quiz-pro' ).'</strong> <input type="text" value=\'[wp_quiz_pro id="'.trim( $_GET['post'] ).'"]\' readonly="readonly" /></div>';
		}
	}

	public function save_post( $post_id, $post ){

		if ( ! wp_verify_nonce( filter_input( INPUT_POST, 'quiz_nonce' ), 'quiz-content' ) )
			return $post_id;

		$post_type = get_post_type_object( $post->post_type );

		if ( ! current_user_can( $post_type->cap->edit_post, $post_id ) )
			return $post_id;

		$quiz_type = get_post_meta( $post_id, 'quiz_type', true );
		$new_quiz_type = filter_input( INPUT_POST, 'quiz_type', FILTER_SANITIZE_STRING );
		if ( $new_quiz_type && ( '' === $quiz_type || $quiz_type !== $new_quiz_type ) ) {
			update_post_meta( $post_id, 'quiz_type', $new_quiz_type );
		}

		//todo: sanitize all inputs
		$settings = $this->prepare_checkbox_options( $_POST[ 'settings' ] );
		$questions = '';
		if( isset( $_POST[ 'questions' ] ) ){
			$questions = array_values( $_POST[ 'questions' ] );
			foreach( $questions as $key => $question ){
				if( isset( $questions[ $key ][ 'answers' ] ) )
					$questions[ $key ][ 'answers' ]  = array_values( $question[ 'answers' ] );
			}
		}

		update_post_meta( $post_id, 'questions', $questions );
		if( isset( $_POST[ 'results' ] ) && !empty( $_POST[ 'results' ] ) )
			update_post_meta( $post_id, 'results', $_POST[ 'results' ] );
		update_post_meta( $post_id, 'settings', $settings );

	}

	public function prepare_checkbox_options( $post ){

		$settings_key = array( 'rand_questions', 'rand_answers', 'restart_questions', 'promote_plugin', 'embed_toggle', 'show_ads', 'show_countdown', 'timer', 'auto_scroll', 'repeat_ads' );
		foreach(  $settings_key as  $key ){
			if( isset( $post[ $key ] )  && $post[ $key ] == "1" )
					$post[ $key ] = 1;
			else
				$post[ $key ] = 0;
		}
		return $post;
	}

	public function add_new_wp_quiz_columns( $columns ) {
		$settings = get_option( 'wp_quiz_pro_default_settings' );

		$new_columns['cb']          =   '<input type="checkbox" />';
		$new_columns['title']       =   __( 'Quiz Name', 'wp-quiz-pro' );
		$new_columns['shortcode']   =   __( 'Shortcode', 'wp-quiz-pro' );
		$new_columns['embed']       =   __( 'Embed Code', 'wp-quiz-pro' );

		if ( isset( $settings['players_tracking'] ) && 1 === $settings['players_tracking'] ) {
			$new_columns['plays']   =   __( 'Plays', 'wp-quiz-pro' );
		}

		$new_columns['type']        =   __( 'Quiz type', 'wp-quiz-pro' );
		$new_columns['date']        =   __( 'Date', 'wp-quiz-pro' );

		return $new_columns;

	}

	public function manage_wp_quiz_columns( $column_name, $id ){
		global $wpdb;
		$type = get_post_meta( $id, 'quiz_type', true );
		switch ($column_name) {
			case 'shortcode':
				echo '<div class="field"><input type="text" readonly value="[wp_quiz_pro id=&quot;' . $id . '&quot;]" onClick="this.select();" style="width:100%;"></div>';
				break;
			case 'embed':
				$site_url =   get_site_url() . '/?wp_quiz_id=' . $id;
				$iframe   =   '<iframe frameborder="0" width="600" height="800" src="' . $site_url . '"></iframe>';
				echo '<div class="field"><input type="text" readonly value="' . htmlentities( $iframe ) . '" onClick="this.select();" style="width:100%;"></div>';
				break;
			case 'plays':
				$play_count = $wpdb->get_var( "SELECT COUNT(*) FROM `" . $wpdb->prefix . "wp_quiz_players` WHERE  pid='" . $id . "'" );
				$fb_play_count = $wpdb->get_var( "SELECT COUNT(*) FROM `" . $wpdb->prefix . "wp_quiz_fb_plays` WHERE  pid='" . $id . "'" );
				if( $type == 'flip' )
					$play_count = 'n/a';
				else if ($type == 'fb_quiz' )
					$play_count = $fb_play_count;
				echo $play_count;
				break;
			case 'type':
				if( $type ) echo ucfirst( str_replace( '_', ' ', $type ) );
				break;
			default:
				break;
		} // end switch

	}

	/**
	 * Add the config page
     */
	public function register_admin_menu() {

		global $wp_quiz_page_hook;
		$page_hook = add_submenu_page( 'edit.php?post_type=wp_quiz' , __( 'General Settings', 'wp-quiz-pro' ), __( 'General Settings', 'wp-quiz-pro' ), 'manage_options', 'wp_quiz_config', array( 'WP_Quiz_Pro_Page_Config', 'page' ) );
		add_action( 'load-' . $page_hook, array( 'WP_Quiz_Pro_Page_Config', 'load' ) );
		add_action( 'admin_print_styles-' . $page_hook, array( 'WP_Quiz_Pro_Page_Config', 'admin_print_styles' ) );

		do_action( 'wp_quiz_admin_page' );
	}

	/**
     * Add the players page
     */
    public function admin_page_players() {

		$settings = get_option( 'wp_quiz_pro_default_settings' );
		if ( isset( $settings['players_tracking'] ) && 1 === $settings['players_tracking'] ) {
			$page_hook = add_submenu_page( 'edit.php?post_type=wp_quiz', __( 'Players', 'wp-quiz-pro' ), __( 'Players', 'wp-quiz-pro' ), 'manage_options', 'wp_quiz_players', array( 'WP_Quiz_Pro_Page_Players', 'page' ) );
			add_action( 'load-' . $page_hook, array( 'WP_Quiz_Pro_Page_Players', 'load' ) );
			add_action( 'admin_print_styles-' . $page_hook, array( 'WP_Quiz_Pro_Page_Players', 'admin_print_styles' ) );
		}
	}

	/**
     * Add the email subscribers page
     */
    public function admin_page_email_subs() {

		$page_hook = add_submenu_page( 'edit.php?post_type=wp_quiz', __( 'E-mail Subscribers', 'wp-quiz-pro' ), __( 'E-mail Subscribers', 'wp-quiz-pro' ), 'manage_options', 'wp_quiz_email_subs', array( 'WP_Quiz_Pro_Page_Email_Subs', 'page' ) );
		add_action( 'load-' . $page_hook, array( 'WP_Quiz_Pro_Page_Email_Subs', 'load' ) );
		add_action( 'admin_print_styles-' . $page_hook, array( 'WP_Quiz_Pro_Page_Email_Subs', 'admin_print_styles' ) );

	}

	/**
     * Add the import/export page
     */
    public function admin_import_export() {

		$page_hook = add_submenu_page( 'edit.php?post_type=wp_quiz', __( 'Import/Export', 'wp-quiz-pro' ), __( 'Import/Export', 'wp-quiz-pro' ), 'manage_options', 'wp_quiz_ie', array( 'WP_Quiz_Pro_Page_Import_Export', 'page' ) );
		add_action( 'load-' . $page_hook, array( 'WP_Quiz_Pro_Page_Import_Export', 'load' ) );
		add_action( 'admin_print_styles-' . $page_hook, array( 'WP_Quiz_Pro_Page_Import_Export', 'admin_print_styles' ) );

	}

	public function on_screen_layout_columns( $columns, $screen_id ){

		if ( $screen_id == 'wp_quiz_page_wp_quiz_config' ) {
			$columns['wp_quiz_page_wp_quiz_config'] = 2;
		} else if ( $screen_id == 'wp_quiz_page_wp_quiz_ie' ) {
			$columns['wp_quiz_page_wp_quiz_ie'] = 2;
		}
		return $columns;
	}

	/**
     * Shortcode used to display quiz
     *
     * @return string HTML output of the shortcode
     */
	public function register_shortcode( $atts ) {

		if ( !isset( $atts['id'] ) ) {
			return false;
		}

		// we have an ID to work with
        $quiz = get_post( $atts['id'] );

        // check if ID is correct
        if ( !$quiz || $quiz->post_type != 'wp_quiz' ) {
            return "<!-- wp_quiz {$atts['id']} not found -->";
        }

		// lets go
       $this->set_quiz( $atts['id'] );
	   $this->quiz->enqueue_scripts();

       return  $this->quiz->render_public_quiz();
	}

	/**
     * Initialise translations
     */
	public function load_plugin_textdomain() {

		load_plugin_textdomain( 'wp-quiz-pro', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	}

	/**
     * Set the current quiz
     */
	public function set_quiz( $id ) {
		$quiz_type = get_post_meta( $id, 'quiz_type', true );
		$this->quiz = $this->create_quiz( $quiz_type, $id );

	}

	/**
	 * Called on save form. Only POST allowed.
	 */
	public function save_post_form() {

		//Allowed Pages
		if ( ! in_array( $_POST[ 'page' ], array ( 'wp_quiz_config' ) ) )
			wp_die( __( 'Cheating, huh?', 'wp-quiz-pro' ) );

		//nonce check
		check_admin_referer( $_POST[ 'page' ] . '_page' );

		if ( ! current_user_can( 'manage_options' ) )
			wp_die( __( 'Cheating, huh?', 'wp-quiz-pro' ) );

		$location = '';

		//Call method to save data
		if ( $_POST[ 'page' ] == 'wp_quiz_config' ){
			WP_Quiz_Pro_Page_Config::save_post_form();
			$location = admin_url() . 'edit.php?post_type=wp_quiz&page=wp_quiz_config';
		}
		//Back to topic
		$location = add_query_arg( 'message', 3, $location );
		wp_safe_redirect( $location );
		exit;
	}

	/**
     * Create a new quiz based on the quiz type
     */
	private function create_quiz( $type, $id ) {
		switch ( $type ) {
			case 'trivia':
				return new WP_Quiz_Pro_Trivia_Quiz( $id );
			case 'personality':
				return new WP_Quiz_Pro_Personality_Quiz( $id );
			case 'swiper':
				return new WP_Quiz_Pro_Swiper_Quiz( $id );
			case 'flip':
				return new WP_Quiz_Pro_Flip_Quiz( $id );
			case 'fb_quiz':
				return new WP_Quiz_Pro_Fb_Quiz( $id );
			default:
				break;
		} // end switch

	}

	public function create_quiz_page( $content ){

		global $post;

		if( $post->post_type != "wp_quiz" ){
			return $content;
		}

		if( !is_single() ){
			return $content;
		}

		$quiz_html = $this->register_shortcode( array( 'id' => $post->ID ) );
		return $quiz_html . $content;

	}

	public function wp_quiz_pro_results(){

		if( !wp_verify_nonce( $_POST[ '_nonce' ], 'ajax-quiz-content' ) )
			return;

		$correct        =   isset( $_POST[ 'correct' ] ) ? absint( $_POST[ 'correct' ] ) : 0;
		$rid            =   isset( $_POST[ 'rid' ] )  ?   $_POST[ 'rid' ] : '';
		$pid            =   absint( $_POST[ 'pid' ] );
		$type           =   sanitize_text_field( $_POST[ 'type' ] );
		$user_ip        =   $this->wp_quiz_pro_get_ip();
		$user_ID        =   get_current_user_id();
		$user_info      =   get_userdata( $user_ID );
		$username       =   is_user_logged_in() ? $user_info->user_login : 'Guest';
		$result         =   '';

		$results = get_post_meta( $pid, 'results', true );

		if( $type == "trivia" ){
			$rid = "";
			foreach( $results as $result ){
				if( $result[ 'min' ] <= $correct && $result[ 'max' ] >= $correct ){
					$result          =   $result[ 'title' ];
					break;
				}
			}
		} else if ( $type == "personality" ){
			for( $i = 0; $i < count( $results ); $i++ ){
				if( $i == $rid ){
					$result          =   $results[ $i ][ 'title' ];
					break;
				}
			}
		} else if( $type == "swiper" ){
			$results = $_POST[ 'results' ];
			$questions 	= get_post_meta( $pid, 'questions', true );
			foreach( $questions as $q_key => $question ){
				foreach( $results as $key => $result ){
					if( $question[ 'uid' ] == $key ){
						if( $result == '0' )
							$questions[ $q_key ][ 'votesDown' ] = $question[ 'votesDown' ] + 1;
						else
							$questions[ $q_key ][ 'votesUp' ] = $question[ 'votesUp' ] + 1;
					}
				}
			}
			update_post_meta( $pid, 'questions', $questions );
			$result = '';
		}

		// Save Result
		$settings = get_option( 'wp_quiz_pro_default_settings' );
		if ( isset( $settings['players_tracking'] ) && 1 === $settings['players_tracking'] ) {
			global $wpdb;
			$wpdb->insert(
				$wpdb->prefix . 'wp_quiz_players',
				array(
					'pid'               =>   $pid,
					'date'       		=>   date( 'Y-m-d', time() ),
					'user_ip'           =>   $user_ip,
					'username'          =>   $username,
					'correct_answered'  =>   $correct,
					'result'            =>   $result,
					'quiz_type'         =>   $type
				),
				array( '%d', '%s', '%s', '%s', '%d', '%s', '%s' )
			);
		}

		die( 'SUCCESS!' );
	}

	public function wp_quiz_pro_user_info(){

		if( !wp_verify_nonce( $_POST[ '_nonce' ], 'ajax-quiz-content' ) )
			return;

		$output = array( 'status' => 1 );

		if ( is_email( $_POST[ 'email' ] ) ) {

			global $wpdb;
			$username		=	sanitize_text_field( $_POST[ 'username' ] );
			$email          =   sanitize_email( $_POST[ 'email' ] );
			$pid            =   absint( $_POST[ 'pid' ] );

			$this->wp_quiz_subscribe_pro( $pid, $username, $email );

			$sql = "SELECT * FROM {$wpdb->prefix}wp_quiz_emails WHERE email = '".$email."'";
			$result = $wpdb->get_results( $sql );

			if ( !$result ) {
				//Save info
				$wpdb->insert(
					$wpdb->prefix . 'wp_quiz_emails',
					array(
						'pid'      	=>   $pid,
						'username'  =>   $username,
						'email'     =>   $email,
						'date'     	=>   date( 'Y-m-d', time() ),
					),
					array( '%d', '%s', '%s', '%s' )
				);
			}

			$output[ 'status' ] = 2;
		}

		wp_send_json( $output );
	}


	public function wp_quiz_pro_fb_user_info(){
		if( !wp_verify_nonce( $_POST[ '_nonce' ], 'ajax-quiz-content' ) )
			return;

		$output = array( 'status' => 1 );
		if ( !empty( $_POST[ 'user' ] ) ){
			global $wpdb;
			$user = $_POST[ 'user' ];
			$nation = "";
			if (isset($user['location'])){
			  $nation = $user['location']['name'];
			}

			$sql = "SELECT * FROM {$wpdb->prefix}wp_quiz_fb_users WHERE uid = '".$user[ 'id' ]."'";

			$result = $wpdb->get_row( $sql );
			if( !$result ){
				$wpdb->insert(
					$wpdb->prefix . 'wp_quiz_fb_users',
					array(
						'uid'			=> absint( $user[ 'id' ] ),
						'email'			=> isset( $user[ 'email' ] ) ? $user[ 'email' ] : '',
						'first_name'	=> $user[ 'first_name' ],
						'last_name'		=> $user[ 'last_name' ],
						'gender'		=> isset( $user[ 'gender' ] ) ? $user[ 'gender' ] : '',
						'picture'		=> isset( $user[ 'picture' ] ) ? $user[ 'picture' ] : '',
						'friends'		=> isset( $user[ 'friends' ] ) ? serialize( $user[ 'friends' ] ) : '',
						'created_at'	=> date( 'Y-m-d', time() ),
						'updated_at'	=> date( 'Y-m-d', time() )
					),
					array( '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' )
				);

				$user[ 'insert_id' ] = $wpdb->insert_id;
			} else {
				$user[ 'insert_id' ] = $result->id;
			}

			if ( $_POST[ 'profile' ] == 'user' )
			{

				$return = $this->wp_quiz_generate_result_user_image( $_POST[ 'pid' ], $user );
			}else{
				$return = $this->wp_quiz_generate_result_friend_image( $_POST[ 'pid' ], $user );
			}

			if( !empty( $return[ 'src' ] ) ){
				$output[ 'src' ] 	= $return[ 'src' ];
				$output[ 'desc' ] 	= $return[ 'desc' ];
				$output[ 'user' ] 	= $nation;
				$output[ 'status' ]	= 2;

				$sql = "SELECT * FROM {$wpdb->prefix}wp_quiz_emails WHERE email = '".$user[ 'email' ]."'";
				$result = $wpdb->get_results( $sql );

				if ( !$result ) {
					//Save info
					$wpdb->insert(
						$wpdb->prefix . 'wp_quiz_emails',
						array(
							'pid'      	=>   $_POST[ 'pid' ],
							'username'  =>   $user[ 'name'],
							'email'     =>   $user[ 'email' ],
							'nation'	=>   $nation,
							'interests'	=>   '',
							'date'     	=>   date( 'Y-m-d', time() ),
						),
						array( '%d', '%s', '%s', '%s','%s','%s' )
					);
				}
			} else {
				$output[ 'error' ]	= $return[ 'error' ];
			}
		}
		sleep(15);
		wp_send_json( $output );
	}

	//method written by francisco
	public function wp_quiz_friend_numbers_from_result($result){
		$n = 0;
		$friend1 = "%%1friend%%";
		$friend2 = "%%2friends%%";
		$friend3 = "%%3friends%%";
		$friend4 = "%%4friends%%";
		if(strpos( $result[ 'desc' ], $friend1 ) !== false){ $n = 1;}
		if(strpos( $result[ 'desc' ], $friend2 ) !== false){ $n = 2;}
		if(strpos( $result[ 'desc' ], $friend3 ) !== false){ $n = 3;}
		if(strpos( $result[ 'desc' ], $friend4 ) !== false){ $n = 4;}
		return $n;
	}
	//method written by francisco
	public function wp_quiz_select_friends($user, $nfriends){
		$friends = $user[ 'friends' ];
		if($nfriends > count($friends)){$nfriends = count($friends);}
		return array_slice($friends,0,$nfriends);
	}

	public function wp_quiz_generate_result_user_image( $post_id, $user ){
		global $wpdb;

		$return 	= array();
		$results 	= get_post_meta( $post_id, 'results', true );
		if( extension_loaded( 'imagick' ) && !empty( $results ) ) {

			$index 	= array_rand( $results );
			$result	= $results[ $index ];
			$friendNumb = $this->wp_quiz_friend_numbers_from_result($result);
			//primer paso para resultado con mas amigos
			$friends    = $this->wp_quiz_select_friends($user, $friendNumb);

			$sql = "SELECT * FROM {$wpdb->prefix}wp_quiz_fb_plays WHERE user_id = '".$user[ 'insert_id' ]."' AND pid = '".$post_id."'";
			$play = $wpdb->get_row( $sql );

			if( !$play ){
				$wpdb->insert(
					$wpdb->prefix . 'wp_quiz_fb_plays',
					array(
						'user_id'	=> absint( $user[ 'insert_id' ] ),
						'pid'		=> absint( $post_id ),
					),
					array( '%d', '%d' )
				);
			}
			$names = array(
				'user_first_name'	=> $user[ 'first_name' ],
				'user_last_name'	=> $user[ 'last_name' ],
				'friend_first_name'	=> '',
				'friend_last_name'	=> ''
			);

			$profile = 'https://graph.facebook.com/'.$user[ 'id' ].'/picture?width=320&height=320';
			$profile = $this->getRedirectUrl( $profile ) ;

			// segundo paso para resultado con mas amigos
			$data 	= $this->wp_quiz_generate_fb_result($post_id, $result, $profile, $names, $friends );
			$return	= $data;

		}

		return $return;
	}

	public function wp_quiz_generate_result_friend_image( $post_id, $user ){
		global $wpdb;

		$return 	= array();
		$results 	= get_post_meta( $post_id, 'results', true );
		$friendNumb = $this->wp_quiz_friend_numbers_from_result($result);
		$friends    = $this->wp_quiz_select_friends($user, $friendNumb);
		if( extension_loaded( 'imagick' ) && !empty( $results ) && !empty( $user[ 'friends' ] ) ) {

			$index 	= array_rand( $results );
			$result	= $results[ $index ];

			$index_2	= array_rand( $user[ 'friends' ] );
			$friend		= $user[ 'friends' ][ $index_2 ];

			$sql = "SELECT * FROM {$wpdb->prefix}wp_quiz_fb_plays WHERE user_id = '".$user[ 'insert_id' ]."' AND pid = '".$post_id."'";
			$play = $wpdb->get_row( $sql );


			if( !$play ) {
				$wpdb->insert(
					$wpdb->prefix . 'wp_quiz_fb_plays',
					array(
						'user_id'	=> absint( $user[ 'insert_id' ] ),
						'pid'		=> absint( $post_id ),
					),
					array( '%d', '%d' )
				);
			}

			$profile = 'https://graph.facebook.com/'.$friend[ 'id' ].'/picture?width=320&height=320';
			$profile = $this->getRedirectUrl( $profile ) ;

			$friend_name = explode( ' ', $friend[ 'name' ] );

			$names = array(
				'user_first_name'	=> $user[ 'first_name' ],
				'user_last_name'	=> $user[ 'last_name' ],
				'friend_first_name'	=> $friend_name[ 0 ],
				'friend_last_name'	=> $friend_name[ 1 ]
			);

			$data 	= $this->wp_quiz_generate_fb_result( $post_id, $result, $profile, $names, $friends );
			$return	= $data;
		}

		return $return;
	}
	//method written by francisco
	public function wp_quiz_friends_images($friends)
	{
		$result =  array();
		$i = 0;
		foreach ($friends as $key => $value) {
			$result[$i]='https://graph.facebook.com/'.$value[ 'id' ].'/picture?width=320&height=320';
			$i++;
		}
		return $result;
	}
	//method written by francisco
	public function result_image_with_friends($result_image, $friends_images, $friends_num, $result)
	{
		$dimension = $result_image->getImageGeometry();
		$width = $dimension['width'];
		$height = $dimension['height'];
		for ($i=0; $i < $friends_num ; $i++) {
		  ${"image_$i"} = new Imagick( $friends_images[$i] );
		  ${"image_$i"}->resizeImage( $result[ 'proImageWidth' ], $result[ 'proImageHeight' ], imagick::FILTER_LANCZOS, 0.9, false );
		}
		switch ($friends_num) {
			case 1:
				$result_image->compositeImage( $image_0, Imagick::COMPOSITE_DEFAULT, $width - $width*0.5 , $height - $height*0.5 );
				break;
			case 2:
				$result_image->compositeImage( $image_0, Imagick::COMPOSITE_DEFAULT, $width - ($width*0.4) , $height - ($height*0.8) );
				$result_image->compositeImage( $image_1, Imagick::COMPOSITE_DEFAULT, $width - ($width*0.4) , $height - ($height*0.4) );
				break;
			case 3:
				$result_image->compositeImage( $image_0, Imagick::COMPOSITE_DEFAULT, $width - ($width*0.9) , $height*0.5 );
				$result_image->compositeImage( $image_1, Imagick::COMPOSITE_DEFAULT, $width - ($width*0.6) , $height*0.5 );
				$result_image->compositeImage( $image_2, Imagick::COMPOSITE_DEFAULT, $width - ($width*0.3) , $height*0.5 );
				break;
			case 4:
				$result_image->compositeImage( $image_0, Imagick::COMPOSITE_DEFAULT, $width - ($width*0.95) , $height - ($height*0.9) );
				$result_image->compositeImage( $image_1, Imagick::COMPOSITE_DEFAULT, $width - ($width*0.45) , $height - ($height*0.9) );
				$result_image->compositeImage( $image_2, Imagick::COMPOSITE_DEFAULT, $width - ($width*0.95) , $height - ($height*0.45) );
				$result_image->compositeImage( $image_3, Imagick::COMPOSITE_DEFAULT, $width - ($width*0.45) , $height - ($height*0.45) );
				break;
			default :
			  $result_image;
		}
		return $result_image;
	}

	public function wp_quiz_generate_fb_result( $post_id, $result, $profile, $names, $friends ){

		$return = array( 'src' => '', 'desc' => '', 'error' => '' );
		$friends_images = $this->wp_quiz_friends_images($friends);
		$friends_num    = count($friends_images);
		try {

			$settings 					= get_post_meta( $post_id, 'settings', true );
			$upload_dir 				= wp_upload_dir();
			$upload_dir[ 'basedir' ]	= $upload_dir[ 'basedir' ] . "/wp_quiz-result-images";
			$upload_dir[ 'baseurl' ] 	= $upload_dir[ 'baseurl' ] . "/wp_quiz-result-images";
			$image_name 				= 'image-' . rand(0, 100000);
			$image_name2 				= rand(0, 100000);
			$image_name3 				= rand(0, 100000);

			$find 		= array( '%%userfirstname%%', '%%userlastname%%', '%%friendfirstname%%', '%%friendlastname%%');
			$replace 	= array( $names[ 'user_first_name' ], $names[ 'user_last_name' ], $names[ 'friend_first_name' ], $names[ 'friend_last_name' ] );
			$title 		= str_replace( $find, $replace, $result[ 'title' ] );
			$desc		= str_replace( $find, $replace, $result[ 'desc' ] );

			$draw = new ImagickDraw();
			$draw->setFillColor( $settings[ 'title_color' ] );
			$draw->setGravity( 1 );
			$draw->setFontSize( $settings[ 'title_size' ] );
			$draw->setFont( $settings[ 'title_font' ] );

			$bg_profile = new Imagick();
			$bg_profile->newImage( $result[ 'proImageWidth' ], $result[ 'proImageHeight' ], new ImagickPixel( 'transparent' ) );

			imagepng( imagecreatefromjpeg( $profile ), $upload_dir[ 'basedir' ] . '/' . $image_name2 . '.png');

			$profile_image = new Imagick( $upload_dir[ 'basedir' ] . '/' . $image_name2 . '.png' );
			$profile_image->resizeImage( $result[ 'proImageWidth' ], $result[ 'proImageHeight' ], imagick::FILTER_LANCZOS, 0.9, false );
			//return 0;
			//$profile_image->roundCorners( 50, 50 );
			$profile_image->compositeImage( $bg_profile, imagick::COMPOSITE_ATOP, 0, 0 );

			$result_imagesize = getimagesize( $result[ 'image' ] );

			imagepng( $this->imagecreatefromfile( $result[ 'image' ], image_type_to_extension( $result_imagesize[ 2 ], false ) ), $upload_dir[ 'basedir' ] . '/' . $image_name3 . '.png' );

			$result_image = new Imagick( $upload_dir[ 'basedir' ] . '/' . $image_name3 . '.png' );

			if( !empty( $title ) ){
				list( $lines, $lineHeight ) = $this->wordWrapAnnotation( $result_image, $draw, $title, $result[ 'titleImageWidth' ] );
				for( $i = 0; $i < count( $lines ); $i++ ){
					$result_image->annotateImage( $draw, $result[ 'pos_title_x' ],  $result[ 'pos_title_y' ] + $i*$lineHeight, 0, $lines[ $i ] );
				}
			}

			$result_image = $this->result_image_with_friends($result_image, $friends_images, $friends_num, $result);

			$result_image->compositeImage( $profile_image, Imagick::COMPOSITE_DEFAULT, $result[ 'pos_x' ], $result[ 'pos_y' ] );

			$result_image->writeImage( $upload_dir[ 'basedir' ] . '/' . $image_name . '.png');

			$bg_profile->destroy();
			$profile_image->destroy();
			$result_image->destroy();

			unlink( $upload_dir[ 'basedir' ] . '/' . $image_name2 . '.png' );
			unlink( $upload_dir[ 'basedir' ] . '/' . $image_name3 . '.png' );
			$return[ 'src' ]	= $upload_dir[ 'baseurl' ] . '/' . $image_name . '.png';
			//$return['src']          = "http://mail.rsgc.on.ca/~ldevir/ICS3U/Chapter4/Images/tux.png";

			$find 		= array(  '%%1friend%%', '%%2friends%%', '%%3friends%%', '%%4friends%%' );
			$replace 	= array('','','','');
			$desc		= str_replace( $find, $replace, $desc );
			$return[ 'desc' ]	= $desc;
		} catch (Exception $ex) {
			$return[ 'error' ] = $ex->getMessage();
		}
		return $return;
	}

	public function imagecreatefromfile( $url, $ext ) {

		switch ( $ext ) {
			case 'jpeg':
			case 'jpg':
				return imagecreatefromjpeg( $url );
			break;

			case 'png':
				return imagecreatefrompng( $url );
			break;

			case 'gif':
				return imagecreatefromgif( $url );
			break;

			default:
				throw new InvalidArgumentException( 'Url "'.$url.'" is not valid jpg, png or gif image.' );
			break;
		}
	}
	public function getRedirectUrl( $url ) {

		stream_context_set_default( array(
				'http' => array(
				'method' => 'HEAD'
			)
		) );

		$headers = get_headers( $url, 1 );
		if ( $headers !== false && isset($headers['Location'] ) ) {
			return is_array( $headers['Location'] ) ? array_pop($headers['Location'] ) : $headers['Location'];
		}
		return $url;
	}


	public function wordWrapAnnotation( $image, $draw, $text, $maxWidth ) {

		$words = preg_split( '%\s%', $text, -1, PREG_SPLIT_NO_EMPTY );
		$lines = array();
		$i = 0;
		$lineHeight = 0;

		while( count( $words ) > 0 ) {
			$metrics = $image->queryFontMetrics( $draw, implode( ' ', array_slice( $words, 0, ++$i ) ) );
			$lineHeight = max( $metrics['textHeight'], $lineHeight );

			if ( $metrics[ 'textWidth' ] > $maxWidth || count( $words ) < $i ) {
				if ( $i == 1 )
					$i++;

				$lines[] = implode( ' ', array_slice( $words, 0, --$i ) );
				$words = array_slice( $words, $i );
				$i = 0;
			}
		}

		return array( $lines, $lineHeight );
	}

	public function wp_quiz_subscribe_pro( $id, $name, $email ){

		$settings 	= get_post_meta( $id, 'settings', true );
		$options 	= get_option( 'wp_quiz_pro_default_settings' );
		if ( $settings[ 'force_action' ] == '1') {
			if ( $options[ 'mail_service' ] == '1' ){
				$this->wp_quiz_subscribe_mailchimp( $options, $name, $email );
			} else if ( $options[ 'mail_service' ] == '2' ) {
				$this->wp_quiz_subscribe_getresponse( $options, $name, $email );
			}
		}
	}

	private function wp_quiz_subscribe_mailchimp( $options, $name, $email ){

		$mc_api_key		= $options[ 'mailchimp' ][ 'api_key' ];
    	$mc_list_id		= $options[ 'mailchimp' ][ 'list_id' ];
		$double_optin	= apply_filters( 'wp_quiz_mailchimp_double_notification', false );

		$vendor_path = $this->wp_quiz_vendor_path();

		if ( $email && $mc_api_key != null &&  $mc_list_id != null ) {

			try {
				if ( !class_exists( 'Mailchimp' ) )
					require_once( $vendor_path . '/Mailchimp.php' );

				$list = new Mailchimp_Lists( new Mailchimp( $mc_api_key ) );
				$merge_vars = null;
				if ( $name ) {
					$fname = $name;
					$lname = '';
					if ( $space_pos = strpos( $name, ' ' ) ) {
						$fname = substr( $name, 0, $space_pos );
						$lname = substr( $name, $space_pos );
					}
					$merge_vars = array( 'FNAME' => $fname, 'LNAME' => $lname );
				}
				$list->subscribe( $mc_list_id, array( 'email' => $email ), $merge_vars, 'html', (bool) $double_optin, true );

			} catch (Exception $ex) {

			}
		}
	}

	private function wp_quiz_subscribe_getresponse( $options, $name, $email ){

		$gr_api_key = $options[ 'getresponse' ][ 'api_key' ];
    	$gr_list_id = $options[ 'getresponse' ][ 'campaign_name' ];

		$vendor_path = $this->wp_quiz_vendor_path();

		if ( $email && $gr_api_key != null && $gr_list_id != null ) {
			try {
				if ( !class_exists( 'GetResponse' ) )
					require_once( $vendor_path . '/getresponse.php' );

				$api = new GetResponse( $gr_api_key );
				$campaignName		= $gr_list_id;
				$subscriberName		= $name;
				$subscriberEmail	= $email;

				$result 	=  $api->getCampaigns( 'EQUALS', $campaignName );
				$campaigns	= array_keys( (array) $result );
				$campaignId	= array_pop( $campaigns );

				$api->addContact( $campaignId, $subscriberName, $subscriberEmail );
			} catch (Exception $ex) {

			}
		}
	}

	public function wp_quiz_vendor_path(){

		return plugin_dir_path( __FILE__ ) . 'vendor';
	}

	public function wp_quiz_pro_check_image_file(){

		$output = array( 'status' => 1 );
		$check = false;
		if( @getimagesize( $_POST[ 'url' ] ) )
			$check = true;

		$output[ 'check' ] = $check;

		wp_send_json( $output );
	}

	public function wp_quiz_pro_check_video_file(){

		$output = array( 'status' => 1 );
		$check = false;
		$id = $_POST[ 'video_id' ];
		$url = "http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=$id&format=json";
		$headers = get_headers( $url );
		if( substr( $headers[0], 9, 3 ) !== "404" ){
			$check = true;
		}

		$output[ 'check' ] = $check;
		wp_send_json( $output );
	}

	public static function activate_plugin(){

		// Don't activate on anything less than PHP 5.4.0 or WordPress 3.4
		if ( version_compare( PHP_VERSION, '5.4.0', '<' ) || version_compare( get_bloginfo( 'version' ), '3.4', '<' ) || ! function_exists( 'spl_autoload_register' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
			deactivate_plugins( basename( __FILE__ ) );
			wp_die( __( 'WP Quiz Pro requires PHP version 5.4.0 with spl extension or greater and WordPress 3.4 or greater.', 'wp-quiz-pro' ) );
		}

		//Dont't activate if wp quiz is active
		if( defined( 'WP_QUIZ_VERSION' ) ){
			deactivate_plugins( basename( __FILE__ ) );
			wp_die( __( 'Please deactivate WP Quiz plugin first to use the Premium features!', 'wp-quiz-pro' ) );
		}

		include( 'inc/activate-plugin.php' );

	}

	public function wp_quiz_pro_get_ip(){

		//Just get the headers if we can or else use the SERVER global
		if ( function_exists( 'apache_request_headers' ) ) {
			$headers = apache_request_headers();
		} else {
			$headers = $_SERVER;
		}

		//Get the forwarded IP if it exists
		if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
			$the_ip = $headers['X-Forwarded-For'];
		} else if( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
			$the_ip = $headers['HTTP_X_FORWARDED_FOR'];
		} else {
			$the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
		}
		return $the_ip;

	}

	public function wp_quiz_imagick_admin_notice(){

		if( !extension_loaded( 'imagick' ) && !get_option( 'wp_dismiss_imagick_notice' ) ){
			?>
				<div id="message" class="wp-quiz-notice error notice is-dismissible">
					<p><strong><?php _e( 'WP Quiz Pro Notice: ', 'wp-quiz-pro' ) ?></strong><?php _e( 'PHP imagick extension is missing, please install this extention to enable the Facebook Quiz type.', 'wp-quiz-pro' ) ?></p>
				</div>
				<script>
					jQuery(document).on( 'click', '.wp-quiz-notice .notice-dismiss', function() {
						//alert( 'hello' );
						jQuery.ajax({
							url: ajaxurl,
							data: {
								action: 'dismiss_imagick_notice'
							}
						})
					});
				</script>
			<?php
		}
	}

	public function wp_quiz_pro_dismiss_imagick_notice(){
		add_option( 'wp_dismiss_imagick_notice', 'true' );
	}

	public function wp_quiz_pro_head(){
		$this->head_script();
	}

	public function head_script(){
		$settings = get_option( 'wp_quiz_pro_default_settings' );
		?>
			<script>
			<?php
				if ( !empty( $settings[ 'analytics' ][ 'tracking_id' ] ) ){
			?>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', '<?php echo $settings[ 'analytics' ][ 'tracking_id' ] ?>', 'auto');
				ga('send', 'pageview');
			<?php
				}
				if ( !empty( $settings[ 'defaults' ][ 'fb_app_id' ] ) ){
			?>
				jQuery(function(){
				window.fbAsyncInit = function() {
					FB.init({
						appId    : <?php echo $settings[ 'defaults' ][ 'fb_app_id' ] ?>,
						xfbml    : true,
						version  : 'v2.7'
					});

					FB.getLoginStatus(function(response) {
						console.log(response);
						getLogin(response);
					});
				};

				(function(d, s, id){
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) {return;}
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js#version=v2.7";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			});


			<?php
				}
			?>
			</script>
		<?php
		if ( is_singular( array('wp_quiz') ) && isset( $settings['share_meta'] ) && 1 === $settings['share_meta'] ) {
			global $post, $wpseo_og;
			$twitter_desc = $og_desc = str_replace( array( "\r", "\n" ), '', strip_tags( $post->post_excerpt ) );
			if( defined( 'WPSEO_VERSION' ) ){
				remove_action( 'wpseo_head', array( $wpseo_og, 'opengraph' ), 30 );
				remove_action( 'wpseo_head', array( 'WPSEO_Twitter', 'get_instance' ), 40 );
				//use description from yoast
				$twitter_desc 	= get_post_meta( $post->ID, '_yoast_wpseo_twitter-description', true );
				$og_desc		= get_post_meta( $post->ID, '_yoast_wpseo_opengraph-description', true );
			}
			?>
			<meta name="twitter:title" content="<?php echo get_the_title(); ?>">
			<meta name="twitter:description" content="<?php echo $twitter_desc; ?>">
			<meta name="twitter:domain" content="<?php echo esc_url( site_url() ); ?>">
                        <meta property="og:type" content="Article" />
			<meta property="og:url" content="<?php the_permalink(); ?>" />
			<meta property="og:title" content="nuevo titulo" />
			<meta property="og:description" content="<?php echo $og_desc; ?>" />
			<?php
			if ( has_post_thumbnail() ) {
				$thumb_id = get_post_thumbnail_id();
				$thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'full', true );
				$thumb_url = $thumb_url_array[0];
				?>
				<meta name="twitter:card" content="summary_large_image">
				<meta name="twitter:image:src" content="<?php echo $thumb_url; ?>">
				<meta property="og:image" content="<?php echo $thumb_url; ?>" />
				<meta itemprop="image" content="<?php echo $thumb_url; ?>">
			<?php
			}
		}
	}

	public function wp_quiz_embed_quiz(){

		if( !isset( $_GET['wp_quiz_id'] ) ) {
			return;
		}

		$qid 			= absint( $_GET[ 'wp_quiz_id'] );
		$quizHTML 		= $this->register_shortcode( array( 'id' => $qid ) );
		$settings 		= get_post_meta( $qid, 'settings', true );
		if( empty( $quizHTML ) ){
			return;
		}
		?>
			<link rel='stylesheet' href='<?php echo WP_QUIZ_PRO_ASSETS_URL . 'css/main.css'; ?>' type='text/css' media='all' />
			<link rel='stylesheet' href='<?php echo WP_QUIZ_PRO_ASSETS_URL . 'css/transition.min.css'; ?>' type='text/css' media='all' />
			<link rel='stylesheet' href='<?php echo WP_QUIZ_PRO_ASSETS_URL . 'css/embed.min.css'; ?>' type='text/css' media='all' />
			<style>
				.wq_embedToggleQuizCtr{ display: none; }
			</style>
		<?php
		if( $settings[ 'skin' ] == 'traditional' ){
			?>
				<link rel='stylesheet' href='<?php echo WP_QUIZ_PRO_ASSETS_URL . 'css/traditional-skin.css'; ?>' type='text/css' media='all' />
			<?php
		} else if( $settings[ 'skin' ] == 'flat' ){
			?>
				<link rel='stylesheet' href='<?php echo WP_QUIZ_PRO_ASSETS_URL . 'css/flat-skin.css'; ?>' type='text/css' media='all' />
			<?php
		}
		$this->head_script();
		?>
			<script>
				var wq_l10n = {"correct": "Correct !", "wrong": "Wrong !","captionTrivia":"You got %%score%% out of %%total%%","captionTriviaFB":"I got %%score%% out of %%total%%, and you?","youVoted":"You voted","nonce": "<?php echo wp_create_nonce( 'ajax-quiz-content' ) ?>"};
			</script>
		<?php

		echo '<div class="wq_embed">' . $quizHTML . '</div>';
		?>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script src="<?php echo WP_QUIZ_PRO_ASSETS_URL . 'js/embed.min.js'; ?>"></script>
			<script src="<?php echo WP_QUIZ_PRO_ASSETS_URL . 'js/transition.min.js'; ?>"></script>
			<script src="<?php echo WP_QUIZ_PRO_ASSETS_URL . 'js/jquery.flip.min.js'; ?>"></script>
			<script src="<?php echo WP_QUIZ_PRO_ASSETS_URL . 'js/hammer.min.js'; ?>"></script>
			<script src="<?php echo WP_QUIZ_PRO_ASSETS_URL . 'js/dynamics.min.js'; ?>"></script>
			<script src="<?php echo WP_QUIZ_PRO_ASSETS_URL . 'js/jquery.jTinder.min.js'; ?>"></script>
			<script src="<?php echo WP_QUIZ_PRO_ASSETS_URL . 'js/main.min.js'; ?>"></script>
		<?php
		die();
	}
}

endif;

add_action( 'plugins_loaded', array( 'WP_Quiz_Pro_Plugin', 'init' ), 10 );
register_activation_hook( __FILE__, array( 'WP_Quiz_Pro_Plugin', 'activate_plugin' ) );
