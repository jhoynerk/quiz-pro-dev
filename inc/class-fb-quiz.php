<?php
/**
 * Override parent 'WP_Quiz_Pro' class with facebook quiz specific markup,
 * 
 */
class WP_Quiz_Pro_Fb_Quiz extends WP_Quiz_Pro {
	
	/**
     * Constructor
     */
    public function __construct( $id  ) {
		
		parent::__construct( $id  );
		add_filter( 'wp_quiz_data_attrs', array( $this, 'add_fb_data_attrs' ) );
	}
	
	public function get_html_questions(){
		
		$questionsHTML 	= '';
		
		if( !empty( $this->questions ) ){
			foreach( $this->questions as $key => $question ){
				
				$desc = !empty( $question[ 'desc' ] ) ? '<p class="desc">' . $question[ 'desc' ] . '</p>' : '';
				$questionsHTML .= '
					<div class="wq_singleQuestionWrapper wq_IsFb" style="">
						<div class="wq_loader-container" style="display:none;">
							<div class="wq_loader_text">
								<img src="'.WP_QUIZ_PRO_ASSETS_URL . 'image/image_spinner.gif" style="width: 64px !important; margin-left: auto; margin-right: auto;" />
								<h3 id="wq_text_loader">' . __( 'Espera unos segundos, la respuesta viene enseguida...', 'wp-quiz-pro' ) .'</h3>
							</div>
						</div>
						<div class="wq_questionTextDescCtr" style="color:'.$this->settings[ 'font_color' ].'">
							<h4>' . $question[ 'title' ] . '</h4>
							' . $desc . '
						</div>
						<div class="wq_questionMediaCtr" >
							<div class="wq_questionImage"><img src="' . $question[ 'image' ] . '" /><span>'.$question[ 'imageCredit' ].'</span></div>
						</div>
						<div>
							
						</div>
						<div class="wq_questionLogin"> 
							<p>' . __( 'Please login with Facebook to see your result', 'wp-quiz-pro') . '</p>
							<button class="wq_loginFB"><i class="sprite sprite-facebook"></i><span>' . __( 'Login with Facebook', 'wp-quiz-pro') . '</span></button>
						</div>
					</div>';
			}

		}
		return $questionsHTML;
	}
	
	public function get_html_results(){
		
		$resultsHTML = '';
		$shareHTML = $this->get_html_share();
		if( !empty( $this->results ) ){
			$result = $this->results[ array_rand( $this->results ) ];
			$resultsHTML .= '
				<div style="display:none;" class="wq_singleResultWrapper wq_IsFb">
					<p><img class="wq_resultImg" src=""/></p>
					<div class="wq_resultDesc"></div>
					' . $shareHTML . '
				</div>
			';
		}
		
		return $resultsHTML;
	}
	
	public function add_fb_data_attrs( $data ){		
		$data .= 'data-quiz-profile="' . $this->settings[ 'profile' ] . '" ';
		return $data;
	}
}