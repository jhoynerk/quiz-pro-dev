<?php
/**
 * Class For WP_Quiz_Pro Configuration page
 */
class WP_Quiz_Pro_Page_Config {
	
	public static function admin_print_styles() {
		
		wp_enqueue_style( 'semantic-checkbox-css', WP_QUIZ_PRO_ASSETS_URL . 'css/checkbox.min.css', array(), WP_QUIZ_PRO_VERSION );
		wp_enqueue_style( 'chosen-css', WP_QUIZ_PRO_ASSETS_URL . 'css/chosen.min.css', array(), WP_QUIZ_PRO_VERSION ); 
		?>
			<style type="text/css" media="screen">
				table#quiz_type_settings input[type=checkbox], table#global_settings input[type=checkbox] { float: right; }
				#config-page input[type=text]{ width: 100%; }
				#config-page label{ display: block; font-size: 14px; color: #666; }
				table#quiz_type_settings tr td , table#global_settings tr td { padding: 10px; color: #666; font-size: 14px; padding-left: 0; }
				table#quiz_type_settings tr td .ui, table#global_settings tr td .ui { float: right; }
				table#quiz_type_settings tr td #select, table#quiz_type_settings tr td input, table#global_settings tr td input { border-radius: 2px; width: 100%; }
				table#quiz_type_settings, table#global_settings { border: none; }
				#ad_code_setting .add-new-h2 { top: 0; position: relative; margin-bottom: 5px; float: left; clear: both; }
				.ad_row{ margin-bottom: 25px; }
				.ad_row textarea{ width: 100%; }
				.ad_action a{ text-decoration: none; color: #a00; }
				.ad_action a:hover, .ad_action a:active{ color: red; text-decoration: none; }
				.chosen-container{ float: right; width: 100% !important; }
				.ui.toggle.checkbox .box, .ui.toggle.checkbox label { padding-left: 4.15em!important }
				.mailchimp_row, .getresponse_row{ display: none; }
			</style>
		<?php
	}
	
	public static function save_post_form(){
		
		//set default options if button clicked
		if ( isset( $_POST[ 'submit' ] ) ) {
			
			$settings_key = array( 'rand_questions', 'rand_answers', 'restart_questions', 'promote_plugin', 'embed_toggle', 'show_ads', 'auto_scroll', 'share_meta', 'repeat_ads' );
			foreach(  $settings_key as  $key ){
				if( isset( $_POST[ 'defaults' ][ $key ] )  && $_POST[ 'defaults' ][ $key ] == "1" )
					$_POST[ 'defaults' ][ $key ] = 1;
				else
					$_POST[ 'defaults' ][ $key ] = 0;
			}

			if ( isset( $_POST[ 'players_tracking' ] ) && $_POST[ 'players_tracking' ]== "1" ) {
				$_POST[ 'players_tracking' ] = 1;
			} else {
				$_POST[ 'players_tracking' ] = 0;
			}
			
			$analytics_settings = array(
				'profile_name' => $_POST[ 'profile_name' ],
				'tracking_id' => $_POST[ 'tracking_id' ]
			);

			$mailchimp_settings = array(
				'api_key' => $_POST[ 'mailchimp_key' ],
				'list_id' => $_POST[ 'mailchimp_list_id' ]
			);
			
			$getresponse_settings = array(
				'api_key' => $_POST[ 'getresponse_key' ],
				'campaign_name' => $_POST[ 'getresponse_name' ]
			);
			
			$settings = array(
				'analytics' => $analytics_settings,
				'mail_service' => $_POST[ 'mail_service' ],
				'mailchimp' => $mailchimp_settings,
				'getresponse' => $getresponse_settings,
				'defaults' => $_POST[ 'defaults' ],
				'ad_code' => isset( $_POST[ 'ad_code' ] ) ? stripslashes_deep( $_POST[ 'ad_code' ] ) : "",
				'players_tracking' => $_POST[ 'players_tracking' ]
			);

			update_option( 'wp_quiz_pro_default_settings', $settings );
		}
	}
	
	public static function display_messages(){
		
		$message = false;
		if ( isset( $_REQUEST[ 'message' ] ) && ( $msg = (int) $_REQUEST[ 'message' ] ) ) {
			if (  $msg == 3 )
				$message = __('Settings saved', 'wp-quiz-pro');
		}
		$class = ( isset( $_REQUEST['error'] ) ) ? 'error' : 'updated';
		
		if ( $message ) : 
		?>
			<div id="message" class="<?php echo $class; ?> notice is-dismissible"><p><?php echo $message; ?></p></div>
		<?php 
		endif; 
	
	}
	
	public static function load(){
		
		//needed javascripts to allow drag/drop, expand/collapse and hide/show of boxes
		wp_enqueue_script( 'common' );
		wp_enqueue_script( 'wp-lists' );
		wp_enqueue_script( 'postbox' );
		
		wp_enqueue_script( 'semantic-checkbox-js', WP_QUIZ_PRO_ASSETS_URL . 'js/checkbox.min.js', array( 'jquery' ), WP_QUIZ_PRO_VERSION, false );
		wp_enqueue_script( 'chosen-js', WP_QUIZ_PRO_ASSETS_URL . 'js/chosen.jquery.min.js', array( 'jquery' ), WP_QUIZ_PRO_VERSION, false );
		
		$screen = get_current_screen();
		add_meta_box( 'google-analytics-content', __( 'Google Analytics', 'wp-quiz-pro' ), array( __CLASS__, 'google_analytics_content' ), $screen->id, 'normal', 'core' );
		add_meta_box( 'mail-service', __( 'Mail Service', 'wp-quiz-pro' ), array( __CLASS__, 'mail_service_content' ), $screen->id, 'normal', 'core' );
		add_meta_box( 'default-quiz-settings', __( 'Default Quiz Settings', 'wp-quiz-pro' ), array( __CLASS__, 'default_settings_content' ), $screen->id, 'normal', 'core' );
		add_meta_box( 'ad-code', __( 'Ad Code', 'wp-quiz-pro' ), array( __CLASS__, 'ad_code_content' ), $screen->id, 'normal', 'core' );
		add_meta_box( 'global-settings', __( 'Global', 'wp-quiz-pro' ), array( __CLASS__, 'global_settings_content' ), $screen->id, 'normal', 'core' );
	}
	
	public static function page(){
		
		$screen = get_current_screen();
		$columns = absint( $screen->get_columns() );
		$columns_css = '';
		if ( $columns ) {
			$columns_css = " columns-$columns";
		} ?>
			<div class="wrap" id="config-page">
				<h2><?php _e( 'General Settings', 'wp-quiz-pro' ); ?></h2>
				<?php self::display_messages(); ?>
				<form action="<?php echo admin_url( 'admin-post.php?action=wp_quiz' ); ?>" method="post">
					<?php wp_nonce_field( 'wp_quiz_config_page' ); ?>
					<?php wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false ); ?>
					<?php wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false ); ?>
					<input type="hidden" name="page" value="wp_quiz_config" />
					<div id="poststuff">
						<div id="post-body" class="metabox-holder <?php echo $columns_css ?>">
							<div id="postbox-container-2" class="postbox-container">
							<?php
								$settings = get_option( 'wp_quiz_pro_default_settings' );
								do_meta_boxes( $screen->id, 'normal', $settings );
							?>
							</div>
							<p class="submit">
								<input type="submit" name="submit" id="submit" class="button-primary" value="<?php _e( 'Save Changes', 'wp-quiz-pro' ); ?>">&nbsp;
								<!-- <input type="submit" name="default_settings" id="default_settings" class="button-secondary" value="Reset all settings to default">-->
							</p>
						</div>
					</div>
				</form>
			</div>
			<script type="text/javascript">
				//<![CDATA[
					jQuery(document).ready( function($) {
						// close postboxes that should be closed
						$('.if-js-closed').removeClass('if-js-closed').addClass('closed');
						// postboxes setup
						postboxes.add_postbox_toggles('<?php echo $screen->id ?>');
						
						$('#new_add_code').on( 'click', function(e){
							e.preventDefault();
							$html = '<p><div class="ad_row"><textarea rows="4" id="input_ad_code" name="ad_code[]" ></textarea><span class="ad_action"><a href="#" onclick="remove_add(this, event);">Delete</a></span</div></p>';
							$('#ad_code_container').append($html);
						});
						remove_add = function(self, e){
							e.preventDefault();
							$(self).parent().parent().remove();
						}
						mail_service_change = function(){
							$('.mailchimp_row, .getresponse_row').hide();
							var val = $('select[name=mail_service]').val();
							if(val == '1'){
								$('.mailchimp_row').show();
							}else if(val == '2'){
								$('.getresponse_row').show();
							}
						}
						mail_service_change();
						$('.ui.toggle').checkbox();
						$('#share_buttons').chosen();
					});
				//]]>
			</script>
		<?php
	}
	
	/**
	 * Html analytics content
     */
	public static function google_analytics_content( $settings ){
		?>
			<table id="quiz_type_settings" class="analytics_content" width="100%" frame="border">
				<tr>
					<td><?php _e( 'Profile Name', 'wp-quiz-pro' ); ?></td>
					<td>
						<input class="ui" name="profile_name" type="text" value="<?php echo $settings[ 'analytics' ][ 'profile_name' ] ?>" >
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Tracking ID', 'wp-quiz-pro' ); ?></td>
					<td>
						<input class="ui" name="tracking_id" type="text" value="<?php echo $settings[ 'analytics' ][ 'tracking_id' ] ?>" >
					</td>
				</tr>
			</table>
		<?php
	}
	
	public static function mail_service_content( $settings ) {
		?>
			<p>
				<select class="ui" id="select" name="mail_service" onchange="mail_service_change()">
					<option value="0" <?php selected( $settings[ 'mail_service' ], "0", true ) ?>><?php _e( 'Select Mail service', 'wp-quiz-pro' ); ?></option>
					<option value="1" <?php selected( $settings[ 'mail_service' ], "1", true ) ?>><?php _e( 'MailChimp', 'wp-quiz-pro' ); ?></option>
					<option value="2" <?php selected( $settings[ 'mail_service' ], "2", true ) ?>><?php _e( 'GetResponse', 'wp-quiz-pro' ); ?></option>
				</select>
			</p>
			<table id="quiz_type_settings"  class="mail_service" width="100%" frame="border">
				<tr class="mailchimp_row">
					<td><?php _e( 'API key', 'wp-quiz-pro' ); ?></td>
					<td>
						<input class="ui" name="mailchimp_key" type="text" value="<?php echo $settings[ 'mailchimp' ][ 'api_key' ] ?>" >
					</td>
				</tr>
				<tr class="mailchimp_row">
					<td><?php _e( 'List ID', 'wp-quiz-pro' ); ?></td>
					<td>
						<input class="ui" name="mailchimp_list_id" type="text" value="<?php echo $settings[ 'mailchimp' ][ 'list_id' ] ?>" >
					</td>
				</tr>
				<tr class="getresponse_row">
					<td><?php _e( 'API key', 'wp-quiz-pro' ); ?></td>
					<td>
						<input class="ui" name="getresponse_key" type="text" value="<?php echo $settings[ 'getresponse' ][ 'api_key' ] ?>" >
					</td>
				</tr>
				<tr class="getresponse_row">
					<td><?php _e( 'Campaign Name', 'wp-quiz-pro' ); ?></td>
					<td>
						<input class="ui" name="getresponse_name" type="text" value="<?php echo $settings[ 'getresponse' ][ 'campaign_name' ] ?>" >
					</td>
				</tr>
			</table>
		<?php
	}
	
	/**
	 * Html default settings content
     */
	public static function default_settings_content( $settings ){
		$settings = $settings[ 'defaults' ];
		?>
			<table id="quiz_type_settings" width="100%" frame="border">
				<tr>
					<td><?php _e( 'Randomize Questions', 'wp-quiz-pro' ); ?></td>
					<td>
						<div class="ui toggle checkbox">
							<input name="defaults[rand_questions]" type="checkbox" value="1" <?php checked( $settings[ 'rand_questions' ], true, true ) ?>>
						</div>
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Randomize Answers', 'wp-quiz-pro' ); ?></td>
					<td>
						<div class="ui toggle checkbox">
							<input name="defaults[rand_answers]" type="checkbox" value="1" <?php checked( $settings[ 'rand_answers' ], true, true ) ?>>
						</div>
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Restart Questions', 'wp-quiz-pro' ); ?></td>
					<td>
						<div class="ui toggle checkbox">
							<input name="defaults[restart_questions]" type="checkbox" value="1" <?php checked( $settings[ 'restart_questions' ], true, true ) ?>>
						</div>
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Promote the plugin', 'wp-quiz-pro' ); ?></td>
					<td>
						<div class="ui toggle checkbox">
							<input name="defaults[promote_plugin]" type="checkbox" value="1" <?php checked( $settings[ 'promote_plugin' ], true, true ) ?>>
						</div>
					</td>
				</tr>
				<?php 
					$desc = empty( $settings['mts_username'] ) ? '<a href="https://mythemeshop.com/#signup" target="_blank">' . __( 'Signup', 'wp-quiz-pro' ) . '</a>' . __( ' and get your referral ID (username) if you don\'t have it already!', 'wp-quiz-pro' ) : __( 'Check your affiliate earning by following ', 'wp-quiz-pro' ) . '<a href="https://mythemeshop.com/go/aff/member/stats" target="_blank">' . __( 'this link', 'wp-quiz-pro' ) . '</a>';
				
				?>
				<tr>
					<td><?php _e( 'MyThemeShop username', 'wp-quiz-pro' ); ?><br /><small><?php echo $desc; ?></small></td>
					<td>
						<input class="ui" name="defaults[mts_username]" type="text" value="<?php echo esc_attr( $settings['mts_username'] ); ?>" >
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Show embed code toggle', 'wp-quiz-pro' ); ?></td>
					<td>
						<div class="ui toggle checkbox">
							<input name="defaults[embed_toggle]" type="checkbox" value="1" <?php checked( $settings[ 'embed_toggle' ], true, true ) ?>>
						</div>
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Share buttons', 'wp-quiz-pro' ); ?></td>
					<td>
						<?php $settings[ 'share_buttons' ] = isset( $settings[ 'share_buttons' ] ) ? $settings[ 'share_buttons' ] : array(); ?>
						<select name="defaults[share_buttons][]" id="share_buttons" data-placeholder="None" multiple>
							<option value="fb" <?php echo in_array( 'fb', $settings[ 'share_buttons' ] ) ? 'selected' : '' ?>><?php _e( 'Facebook', 'wp-quiz-pro' ); ?></option>
							<option value="tw" <?php echo in_array( 'tw', $settings[ 'share_buttons' ] ) ? 'selected' : '' ?>><?php _e( 'Twitter', 'wp-quiz-pro' ); ?></option>
							<option value="g+" <?php echo in_array( 'g+', $settings[ 'share_buttons' ] ) ? 'selected' : '' ?>><?php _e( 'Google +', 'wp-quiz-pro' ); ?></option>
							<option value="vk" <?php echo in_array( 'vk', $settings[ 'share_buttons' ] ) ? 'selected' : '' ?>><?php _e( 'VK', 'wp-quiz-pro' ); ?></option>
						</select>
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Enable or disable Open Graph and Twitter Cards meta tags in single quiz head tag.', 'wp-quiz' ); ?></small></td>
					<td>
						<div class="ui toggle checkbox">
							<input name="defaults[share_meta]" type="checkbox" value="1" <?php checked( $settings[ 'share_meta' ], true, true ) ?>>
						</div>
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Facebook App ID', 'wp-quiz-pro' ); ?></td>
					<td>
						<input class="ui" name="defaults[fb_app_id]" type="text" value="<?php echo $settings[ 'fb_app_id' ] ?>" >	
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Show Ads', 'wp-quiz-pro' ); ?></td>
					<td>
						<div class="ui toggle checkbox">
							<input name="defaults[show_ads]" type="checkbox" value="1" <?php checked( $settings[ 'show_ads' ], true, true ) ?>>
						</div>
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Show Ads after every nth question', 'wp-quiz-pro' ); ?></td>
					<td>
						<input class="ui" name="defaults[ad_nth_display]" type="number" value="<?php echo $settings[ 'ad_nth_display' ] ?>" >	
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Repeat Ads', 'wp-quiz-pro' ); ?></td>
					<td>
						<div class="ui toggle checkbox">
							<input name="defaults[repeat_ads]" type="checkbox" value="1" <?php checked( $settings[ 'repeat_ads' ], true, true ) ?>>
						</div>
					</td>
				</tr>
				<tr>	
					<td><?php _e( 'Countdown timer [Seconds/question]', 'wp-quiz-pro' ) ?><br/><small><?php _e( 'applies to trivia quiz, multiple page layout', 'wp-quiz-pro' ) ?></small></td>
					<td>
						<input class="ui" name="defaults[countdown_timer]" type="number" value="<?php echo $settings[ 'countdown_timer' ] ?>">
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Auto scroll to next question', 'wp-quiz-pro' ); ?><br/><small><?php _e( 'applies to trivia or personality quiz, single page layout', 'wp-quiz-pro' ) ?></small></td>
					<td>
						<div class="ui toggle checkbox">
							<input name="defaults[auto_scroll]" type="checkbox" value="1" <?php checked( $settings[ 'auto_scroll' ], true, true ) ?>>
						</div>
					</td>
				</tr>
				<tr>
					<td><?php _e( 'Force action to see the results', 'wp-quiz-pro' ); ?><br/><small><?php _e( 'applies to trivia or personality quiz', 'wp-quiz-pro' ) ?></small></td>
					<td>
						<select class="ui" id="select" name="defaults[force_action]">
							<option value="0" <?php selected( $settings[ 'force_action' ], "0", true ) ?>><?php _e( 'No Action', 'wp-quiz-pro' ); ?></option>
							<option value="1" <?php selected( $settings[ 'force_action' ], "1", true ) ?>><?php _e( 'Capture Email', 'wp-quiz-pro' ); ?></option>
							<option value="2" <?php selected( $settings[ 'force_action' ], "2", true ) ?>><?php _e( 'Facebook Share', 'wp-quiz-pro' ); ?></option>
						</select>
					</td>
				</tr>
			</table>
		
		<?php
	
	}
	
	/**
	 * Html Ad Code content
     */
	public static function ad_code_content( $settings ){

		?>
		<div id="ad_code_setting">
			<div style="margin-bottom:5px;"><a style="margin-left:0" href="#" id="new_add_code" class="add-new-h2"><?php _e( 'Add New', 'wp-quiz-pro' ); ?></a><br style="clear:both;"></div>
			<div><div id="ad_code_container">
			<?php if( !empty( $settings[ 'ad_code' ] ) ): ?>
					<?php foreach( $settings[ 'ad_code' ] as $ad ): ?>
						<?php if( !empty( $ad ) ): ?>
							<div class="ad_row"><textarea rows="4" name="ad_code[]" id="input_ad_code"><?php echo esc_html( $ad ) ?></textarea><span class="ad_action"><a href="#" onclick="remove_add(this, event);"><?php _e( 'Delete', 'wp-quiz-pro' ); ?></a></span></div>
						<?php endif; ?>
					<?php endforeach; ?>
			<?php endif; ?>
			</div></div>
		</div>
	<?php
	}
	
	public static function global_settings_content( $settings ) {
		?>
		<table id="global_settings" width="100%" frame="border">
			<tr>
				<td><?php _e( 'Enable or disable players tracking.', 'wp-quiz-pro' ); ?></td>
				<td>
					<div class="ui toggle checkbox">
						<input name="players_tracking" type="checkbox" value="1" <?php checked( $settings[ 'players_tracking' ], true, true ) ?>>
					</div>
				</td>
			</tr>
		</table>
	<?php
	}
}