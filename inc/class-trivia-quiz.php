<?php
/**
 * Override parent 'WP_Quiz_Pro' class with trivia quiz specific markup,
 *
 */
class WP_Quiz_Pro_Trivia_Quiz extends WP_Quiz_Pro {

  /**
     * Constructor
     */
    public function __construct( $id  ) {

    parent::__construct( $id  );
    add_filter( 'wp_quiz_output', array( $this, 'add_timer_markup' ) );
    add_filter( 'wp_quiz_data_attrs', array( $this, 'add_trivia_data_attrs' ) );
  }

  public function get_html_questions(){

    $questionsHTML  = '';

    if( !empty( $this->questions ) ){
      if( $this->settings[ 'rand_questions' ] ){
        shuffle( $this->questions );
      }

      $i = 0;
      $show_ads = $this->settings[ 'show_ads' ];
      $repeat_ads = $this->settings[ 'repeat_ads' ];
      $ad_nth = $this->settings[ 'ad_nth_display' ];
      if( !empty ( $this->settings[ 'ad_codes' ] )  ){
        $ad_codes = explode( ",", $this->settings[ 'ad_codes' ] );
        $number_ads = count( $ad_codes );
      }else{
        $ad_codes = $this->ad_codes;
        $number_ads = count( $this->ad_codes );
      }

      if( $this->settings[ 'question_layout' ] == 'single' ){
        $ad_display = 'block';
        $display_continue = 'none';
      }else{
        $ad_display = 'none';
        $display_continue = 'block';
      }
      $id = 0;
      $index_w_img = 0;
      foreach( $this->questions as $key => $question ){
        if( $show_ads && $ad_nth !== "0" && ( ( $key ) % $ad_nth === 0 ) && $key !== 0 ){
          if( !empty( $ad_codes[ $i ] ) && isset( $ad_codes[ $i ] ) ) {

            $questionsHTML         .=  '
              <div class="wq_singleQuestionWrapper wq_IsTrivia wq_isAd" style="display:'.$ad_display.';">
                <p style="font-size:12px;margin-bottom:0;">'.__( 'Advertisement', 'wp-quiz-pro' ).'</p>
                ' . $ad_codes[ $i ] . '
                <div class="wq_continue" style="display:'.$display_continue.';">
                  <button class="wq_btn-continue" style="background-color:'.$this->settings[ 'bar_color' ].'">'.__( 'Continue &gt;&gt;', 'wp-quiz-pro' ).'</button>
                </div>
              </div>
            ';
            $i++;
            if( $number_ads == $i && $repeat_ads )
              $i  = 0;
          }
        }

        $mediaHTML = '';
        if( $question[ 'mediaType' ] == 'image' ){
          if( !empty( $question[ 'image' ] ) ) {
            $mediaHTML = '<div class="wq_questionImage"><img src="' . $question[ 'image' ] . '" /><span>'.$question[ 'imageCredit' ].'</span></div>';
          }
        } else if ( $question[ 'mediaType' ] == 'video' ){
          if( !empty( $question[ 'video' ] ) ){
            if ( preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $question[ 'video' ], $match ) ){
              if( !empty( $match[ 1 ] ) ){
                $mediaHTML = '<div class="ui embed media-'.$key.'" data-source="youtube" data-id="' . $match[ 1 ] . '"  data-placeholder="'.$question[ 'imagePlaceholder' ].'" data-icon="video play"></div>';
              }
            } else if ( preg_match( '#(?:https?://)?(?:www.)?(?:player.)?vimeo.com/(?:[a-z]*/)*([0-9]{6,11})[?]?.*#', $question[ 'video' ], $match ) ) {
              if( !empty( $match[ 1 ] ) ){
                $mediaHTML = '<div class="ui embed media-'.$key.'" data-source="vimeo" data-id="' . $match[ 1 ] . '"  data-placeholder="'.$question[ 'imagePlaceholder' ].'" data-icon="video play"></div>';
              }
            } else {
              $mediaHTML = '<div class="ui embed media-'.$key.'" data-url="'.$question[ 'video' ].'" data-placeholder="'.$question[ 'imagePlaceholder' ].'" data-icon="video play"></div>';
            }
            $autoplay = !empty( $question[ 'imagePlaceholder' ] ) ? 'true':'false';
            $mediaHTML .= '<script>jQuery(document).ready(function($){$(".ui.media-'.$key.'").embed({"autoplay":'.$autoplay.'});});</script>';
          }
        }

        $answersHTML = '';
        if( isset( $question[ 'answers' ] ) ){
          if( $this->settings[ 'rand_answers' ] ) {
            shuffle( $question[ 'answers' ] );
          }
          $answersHTML    =   '<div class="wq_answersWrapper notranslate">';
          $answersHasImage = false;
          foreach( $question[ 'answers' ] as $answer ){
            if( !empty( $answer[ 'image' ] ) ) {
              $answersHasImage = true;
              $answersHTML = '';
              $answersHTML = '<div class="wq_answersWrapper">';
              break;
            }

            $answersHTML .= '
              <div class="wq_singleAnswerCtr wq_IsTrivia" onclick = "build_json('. $id .')" data-crt="' . $answer[ 'isCorrect' ] . '" style="background-color:'.$this->settings[ 'background_color' ].'; color:' . $this->settings[ 'font_color' ] . ';">
                <label class="wq_answerTxtCtr"  id = '. $id .' >'. $answer[ 'title' ] .'</label>
              </div>
              <p hidden id = "no_image'. $id .'">none</p>
              <p hidden id = "question'. $id .'">'. $question[ 'title' ] .'</p>
            ';

          $id ++;
          $index_w_img = $id;
          }


          if( $answersHasImage ){
            $cols = apply_filters( 'wp_quiz_pro_img_answer_cols', 3 ); // 2 columns if anything else is passed
            if ( 3 === $cols ) {
              $col_class = 'col-md-wq-4';
            } else {
              $col_class = 'col-md-wq-6';
              $cols = 2;
            }
            $j=0;
            $answersHTML .= '<div class="row notranslate">';
            foreach( $question[ 'answers' ] as $answer ){
              $answerImgHTML      =   '';
              $answerTitle = '';
              $answerImgHTML = '<div class="wq_answerImgCtr"><img  src="' . $answer[ 'image' ] . '">
              <p hidden id = "w_image'. $index_w_img .'"> ' . $answer[ 'image' ] . '</p> </div>';

              $answerTitle = empty( $answer[ 'title' ] ) ? '&nbsp;' : $answer[ 'title' ];
              $answersHTML .= '
                <div class="'.$col_class.'">
                  <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" onclick = "build_json('. $index_w_img .')" data-crt="' . $answer[ 'isCorrect' ] . '" style="background-color:'.$this->settings[ 'background_color' ].'; color:' . $this->settings[ 'font_color' ] . ';">
                    ' . $answerImgHTML . '
                    <label class="wq_answerTxtCtr" id = '. $index_w_img .' >'. $answerTitle .'</label>
                  </div>
                  <p hidden id = "question'. $index_w_img .'">'. $question[ 'title' ] .'</p>
                </div>
              ';
              $j++;
              if ( $j%$cols == 0 )
                $answersHTML .= '</div><div class="row">';
                 $index_w_img ++;
                 $id = $index_w_img;
            }

            $answersHTML .= '</div>';
          }
          $answersHTML .=   '</div>';
        }


        $display = $key == 0 ? 'block' : 'none';
        if( $this->settings[ 'question_layout' ] == 'single' ){
          $display = 'block';
        }
        $questionsHTML .= '
          <div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:'.$display.';">
            <div class="wq_singleQuestionCtr">
              <div class="wq_questionTextWrapper quiz-pro-clearfix">
                <div class="wq_questionTextCtr" style="background-color:'.$this->settings[ 'background_color' ].'; color:' . $this->settings[ 'font_color' ] . ';">
                  <h4> '.$question[ 'title' ].' </h4>

                </div>
              </div>
              <div class="wq_questionMediaCtr" >
                ' . $mediaHTML . '
              </div>
              <div class="wq_questionAnswersCtr">
                ' . $answersHTML. '
              </div>
              <div class="wq_triviaQuestionExplanation">
                <div class="wq_ExplanationHead"></div>
                <p class="wq_QuestionExplanationText">' . $question[ 'desc' ]. '</p>
              </div>
            </div>
            <div class="wq_continue" style="display:none;">
              <button class="wq_btn-continue" style="background-color:'.$this->settings[ 'bar_color' ].'">'.__( 'Continue &gt;&gt;', 'wp-quiz-pro' ).'</button>
            </div>
          </div>
        ';
      }
    }
    return $questionsHTML;
  }

  public function get_html_results(){
    header("access-control-allow-origin: *");
    $resultsHTML = '';
    $shareHTML = $this->get_html_share();
    if( !empty( $this->results ) ){
      foreach( $this->results as $result ){
        $resultImgHTML = '';
        if( !empty( $result[ 'image' ] ) ){
          $resultImgHTML = '<p><img class="wq_resultImg" src="' . $result[ 'image' ] . '"/></p>';
        }
        $resultsHTML .= '
          <div>PruebaContent</div> <div style="display:none;" class="wq_singleResultWrapper wq_IsTrivia" data-min="' . $result[ 'min' ] . '" data-max="' . $result[ 'max' ] . '">
            <span class="wq_quizTitle">' . get_the_title( $this->id ) . '</span>
            <div class="wq_resultScoreCtr"></div>
            <div class="wq_resultTitle"><strong>' . $result[ 'title' ] . '</strong></div>
            ' . $resultImgHTML .  '
            <div class="wq_resultDesc">' . $result[ 'desc' ] . '</div>
            ' . $shareHTML . '
          </div>
        ';
      }
    }

    $resultsHTML .='<script>
      function include(arr, obj, attr) {
        for(var i=0; i < arr.length; i++) {
          if (arr[i][attr] == obj) return true;
        }
      }

      function summon_ajax(_form) {
        console.log(_form)
          rburl="http://upsocl-analytics-dev.herokuapp.com/api/v1/quizzes_answer.json";
	   json_form = JSON.stringify(_form);
	   json_parsed = JSON.parse(json_form);
           jQuery.post({
            url: rburl,
            dataType: "json",
            type: "POST",
            data: json_parsed,
            crossDomain: true,
	    beforeSend: setHeader,
            complete: function(xhr,statusText) {
            },
            success: function(result){
            console.log("OK");
            },
            error:function(req, status, err){
            console.log("error, revisar consola de ruby");
          }
        });
     }

    function setHeader(xhr) {
	token = md5("upsocltoken");
    	xhr.setRequestHeader("Authorization", token);
    }

    function assign_quiz(get_question,get_answer,get_image){
            quiz = { "question": get_question, "answer": get_answer, "image": get_image }
            _form.quizzes.push(quiz);
    }

    URLactual = window.location;
    var validated_form = null;
    var _form = {
      "quizzes": [],
      "emails": null,
      "Url":[],
      "results":null
                };
    jQuery( document ).ready(function() {
      validated_form = document.getElementById("formulario-upsocl-email");
      if(validated_form){
       jQuery("#formulario-upsocl-email input[type=submit]").click(function(){
        if(jQuery(".wq_singleResultWrapper.wq_IsTrivia.transition.visible").css("display") == "block"){
         if( jQuery("#email").val() != ""){
           _form.emails = jQuery("#email").val();
       _form.results=jQuery(".wq_singleResultWrapper.wq_IsTrivia.transition.visible.wq_resultTitle").text();
           summon_ajax(_form);
           jQuery("#formulario-upsocl-email input[type=submit]").val("Correo enviado...");
         }
        }
       });
      }
    });
    _form.Url.push(URLactual.href);
    console.log("3");
    function build_json(n){

      var get_question = jQuery("#question"+n).text();
 	  get_question = get_question.trimLeft(get_question);
	  get_question = get_question.trimRight(get_question);
      var get_answer = jQuery("#"+ n).text();
 	  get_answer = get_answer.trimLeft(get_answer);
	 get_answer = get_answer.trimRight(get_answer);

      var largo = _form.quizzes.length;
      for (i = 0; i < largo; i++){
        if(_form.quizzes[i].question == get_question){
          _form.quizzes[i].answer = get_answer;
         if(jQuery("#w_image"+ n).text() == ""){
          _form.quizzes[i].image = null;
         }else{
          _form.quizzes[i].image = jQuery("#w_image"+ n).text();
          }
        }
      }
      if( ( include(_form.quizzes, get_question, "question") == undefined ) && ( include(_form.quizzes, get_answer, "answer") == undefined ) && ( include(_form.quizzes, jQuery("#w_image"+ n).text(), "image") == undefined ) ) {
        conditional = true;
      }

      if( (( jQuery("#w_image"+ n).text() == "") || (jQuery("#no_image"+ n).text() == "none") && conditional ) ){
            assign_quiz(get_question,get_answer,null);
      }else{
            assign_quiz(get_question,get_answer,jQuery("#w_image"+ n).text());
      }

    }
</script>';

  return $resultsHTML;

  }

  public function add_timer_markup( $html ){

    if( $this->settings[ 'countdown_timer' ] > 0  && $this->settings[ 'question_layout' ] == 'multiple' ){
      $htmlTimer = '
        <div class="wq_triviaQuizTimerInfoCtr">
          <p>
            ' . __( 'This is a timed quiz. You will be given ', 'wp-quiz-pro' ) . $this->settings[ 'countdown_timer' ] .  __( ' seconds per question. Are you ready?', 'wp-quiz-pro' ) .'
          </p>
          <button type="button" class="wq_beginQuizCtr" style="background-color:'.$this->settings[ 'bar_color' ].'"> ' . __( 'Begin!', 'wp-quiz-pro' ). '</button>
        </div>
        <div class="wq_triviaQuizTimerCtr" style="background-color:'.$this->settings[ 'bar_color' ].'">' . $this->settings[ 'countdown_timer' ] . '</div>
      ';
      return str_replace( array( '<div class="timerPlaceholder"></div>', 'class="wq_quizProgressBarCtr"', 'class="wq_questionsCtr"' ), array( $htmlTimer ,'class="wq_quizProgressBarCtr" style="display:none;"', 'class="wq_questionsCtr" style="display:none;"' ), $html );

    }else{
      return $html;
    }
  }

  public function add_trivia_data_attrs( $data ){

    $endAnswers = isset( $this->settings[ 'end_answers' ] ) && $this->settings[ 'question_layout' ] == 'single' ? 'true' : 'false';
    $data .= 'data-end-answers="' . $endAnswers . '" ';
    $data .= 'data-quiz-timer="' . $this->settings[ 'countdown_timer' ] . '" ';

    return $data;
  }
}
